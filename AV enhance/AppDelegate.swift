//
//  AppDelegate.swift
//  AV enhance
//
//  Created by Apple on 03/08/2019.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        print(documentsDirectory)
        
        //        NSString *resourceFolder = [[NSBundle mainBundle] resourcePath];
        //        NSDictionary *fontNameMapping = @{@"MyFontName" : @"Doppio One"};
        //
        //        [MobileFFmpegConfig setFontDirectory:resourceFolder with:fontNameMapping];
        //        [MobileFFmpegConfig setFontDirectory:resourceFolder with:nil];
        
        
        let tt = Bundle.main.resourcePath!
        let dict = ["MyFontName":"Doppio One"]
        
        MobileFFmpegConfig.setFontDirectory(tt, with: dict)
        MobileFFmpegConfig.setFontDirectory(tt, with: nil)
        
        self.initiateTabBarVC()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let needTo = url.startAccessingSecurityScopedResource()
        
        do {
            let data = try Data(contentsOf: url)
            let contents = String(
                data: data,
                encoding: String.Encoding.utf8
            )
            
            print(contents)
        } catch(let error) { print(error) }
        
        if needTo {
            url.stopAccessingSecurityScopedResource()
        }
        
        return true
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

extension AppDelegate {
    
    func initiateTabBarVC() {
        
        let tabBarController = UITabBarController()
        var controllers = [UIViewController]()
        
        let audioViewController = AudioViewController(nibName: "AudioViewController", bundle: nil)
        
        let videoViewController = VideoViewController(nibName: "VideoViewController", bundle: nil)
        
        let galleryViewController = GalleryViewController(nibName: "GalleryViewController", bundle: nil)
        
        let settingsViewController = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
        
        let settingsImage = UIImage(named: "tab_bar_settings_icon")
        let audioImage = UIImage(named: "tab_bar_audio_icon")
        let videoImage = UIImage(named: "tab_bar_video_icon")
        let galleryImage = UIImage(named: "tab_bar_gallery_icon")
        
        let audioVCTabBarItem = UITabBarItem.init(title: "Audio", image: audioImage, selectedImage: audioImage)
        audioVCTabBarItem.tag = MainTabBarIndex.Audio.rawValue
        audioViewController.tabBarItem = audioVCTabBarItem
        
        let videoVCTabBarItem = UITabBarItem.init(title: "Video", image: videoImage, selectedImage: videoImage)
        videoVCTabBarItem.tag = MainTabBarIndex.Video.rawValue
        videoViewController.tabBarItem = videoVCTabBarItem
        
        let galleryVCTabBarItem = UITabBarItem.init(title: "Gallery", image: galleryImage, selectedImage: galleryImage)
        galleryVCTabBarItem.tag = MainTabBarIndex.Gallery.rawValue
        galleryViewController.tabBarItem = galleryVCTabBarItem
        
        let settingsVCTabBarItem = UITabBarItem.init(title: "Settings", image: settingsImage, selectedImage: settingsImage)
        settingsVCTabBarItem.tag = MainTabBarIndex.Settings.rawValue
        
        settingsViewController.tabBarItem = settingsVCTabBarItem
        
        controllers.append(audioViewController)
        controllers.append(videoViewController)
        controllers.append(galleryViewController)
        controllers.append(settingsViewController)
        
        tabBarController.viewControllers = controllers
        
        tabBarController.selectedIndex = 0
        tabBarController.tabBar.isOpaque = true
        
        tabBarController.delegate = self
        
        self.customiseTabBar(tabBarController: tabBarController)
        
        let navigationController = UINavigationController(rootViewController: tabBarController)
        navigationController.navigationBar.isHidden = true
        self.window?.rootViewController = navigationController
        
    }
    
    func customiseTabBar(tabBarController: UITabBarController) {
        
        tabBarController.tabBar.barTintColor = CommonClass.sharedInstance.whiteColor
        tabBarController.tabBar.tintColor = CommonClass.sharedInstance.themeColor
        tabBarController.tabBar.unselectedItemTintColor = CommonClass.sharedInstance.grayColor
        
    }
    
}

extension AppDelegate: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}

