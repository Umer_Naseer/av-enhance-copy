//
//  MediaTableViewCell.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

class MediaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var formatStackView: UIStackView!
    @IBOutlet weak var audioStackView: UIStackView!
    
    @IBOutlet weak var selectedImageView: UIImageView!
    
    @IBOutlet weak var videoIconImageView: UIImageView!
    @IBOutlet weak var moreImageView: UIImageView!
    
    @IBOutlet weak var videoTypeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var formatLabel: UILabel!
    @IBOutlet weak var audioLabel: UILabel!
    
    @IBOutlet weak var formatButton: UIButton!
    @IBOutlet weak var audioButton: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MediaTableViewCell {
    func bindData(model: MediaItems, searchTerm: String) {
        if model.fullName.count > 0 {
            self.titleLabel.text = model.fullName
        } else{
            self.titleLabel.text = model.videoName
        }
        print("FILENAME = \(model.videoName)")
        
        self.descLabel.text = "\(model.videoDesc) - \(model.videoLength)"
        self.videoTypeLabel.text = model.fileType.capitalized
        self.audioLabel.text = model.soundLevel
        self.formatLabel.text = model.fileTypeToConvert
        if(model.isAudio) {
            self.videoIconImageView.image = UIImage(named: "tab_bar_audio_icon")
        }
        else {
            self.videoIconImageView.image = UIImage(named: "tab_bar_video_icon")
        }
    }
}
