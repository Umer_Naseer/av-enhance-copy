//
//  CommonClass.swift
//  AV enhance
//
//  Created by umer on 8/3/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class CommonClass {
    
    static var sharedInstance = CommonClass()
    
    // All the colors used in the app
    let themeColor = UIColor.init(red: 65/255.0, green: 181/255.0, blue: 208/255.0, alpha: 1.0)
    let whiteColor = UIColor.white
    let grayColor = UIColor.init(red: 62/255.0, green: 62/255.0, blue: 62/255.0, alpha: 1.0)
    
    func setTabBarTitles(tabbarVC: UITabBarController) {
        if let tabTitles = tabbarVC.tabBar.items {
            tabTitles[0].title = Constants.TabBarTitels.Audio
            tabTitles[1].title = Constants.TabBarTitels.Video
            tabTitles[2].title = Constants.TabBarTitels.Gallery
            tabTitles[3].title = Constants.TabBarTitels.Settings
            
            let selectedColor = self.themeColor
        tabTitles[0].setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): selectedColor]), for: .selected)
        tabTitles[1].setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): selectedColor]), for: .selected)
        tabTitles[2].setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): selectedColor]), for: .selected)
            
        tabTitles[3].setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): selectedColor]), for: .selected)
        }
    }
    
    func showPopoverFrom(mediaTableViewCell: MediaTableViewCell, forButton button: UIButton, sheet: UIAlertController, owner: UIViewController, tableView: UITableView) {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            owner.present(sheet, animated: true, completion: nil)
        case .pad:
            /* Get the souce rect frame */
            let buttonFrame = button.frame
            var showRect    = mediaTableViewCell.convert(buttonFrame, to: tableView)
            showRect        = tableView.convert(showRect, to: owner.view)
            showRect.origin.y -= 10
            
            if let popoverPresentationController = sheet.popoverPresentationController {
                popoverPresentationController.permittedArrowDirections = .up
                popoverPresentationController.sourceView = owner.view
                popoverPresentationController.sourceRect = showRect
                owner.present(sheet, animated: true, completion: nil)
            }
        case .unspecified:
            owner.present(sheet, animated: true, completion: nil)
        default:
            print("")
        }
        
    }
    
    func showSheet(sheet: UIAlertController, sender: UIButton, owner: UIViewController, direction: UIPopoverArrowDirection = .up) {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            owner.present(sheet, animated: true, completion: nil)
        case .pad:
            let popOver = sheet.popoverPresentationController
            
            let viewForSource = sender
            popOver?.sourceView = viewForSource
            popOver?.sourceRect = viewForSource.bounds
            popOver?.permittedArrowDirections = direction
            
            owner.present(sheet, animated: true, completion: nil)
        case .unspecified:
            owner.present(sheet, animated: true, completion: nil)
        default:
            print("")
        }
        
    }
    
    func getCurrentDate() -> Date {
        
        let date = Date()
        return date
        
    }
    
    func getCurrentDateInString() -> String {
        
        let locale = Locale.current//init(identifier: "en_US_POSIX")
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm"
        let date = Date()
        return dateFormatter.string(from: date)
        
    }
    
    func getFormattedStringForSeconds(seconds: Double) -> String {
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .short
        
        let formattedString = formatter.string(from: TimeInterval(seconds))!
        
        return formattedString
        
    }
    
    func getSecondsFromString(string: String) -> Double {
        let components = string.components(separatedBy: ",")
        var seconds : Double = 0
        
        for component in components {
            if component.contains("sec"){
                seconds += Double(component.westernArabicNumeralsOnly)!
                /*
                var secs = component.replacingOccurrences(of: " ", with: "")
                secs = secs.replacingOccurrences(of: "secs", with: "")
                seconds = seconds + Double(secs)!*/
            }
            else  if component.contains("min"){
                var mins = component.replacingOccurrences(of: " ", with: "")
                mins = mins.replacingOccurrences(of: "min", with: "")
                seconds = seconds + (Double(mins)! * 60.0)
            }
            else  if component.contains("hr"){
                var hr = component.replacingOccurrences(of: " ", with: "")
                hr = hr.replacingOccurrences(of: "hr", with: "")
                seconds = seconds + (Double(hr)! * 60.0 * 60.0)
            }
            
        }
        return seconds
    }
    
    func getSecondsFromStringVedio(string: String) -> Double {
        let components = string.components(separatedBy: ",")
        var seconds : Double = 0
        
        for component in components {
            if component.contains("sec"){
                
                seconds = Double(component.westernArabicNumeralsOnly)!
                
                /*
                var secs = component.replacingOccurrences(of: " ", with: "")
                secs = secs.replacingOccurrences(of: "secs", with: "")
                seconds = seconds + Double(secs)!*/
            }
            else  if component.contains("min"){
                var mins = component.replacingOccurrences(of: " ", with: "")
                mins = mins.replacingOccurrences(of: "min", with: "")
                seconds = seconds + (Double(mins)! * 60.0)
            }
            else  if component.contains("hr"){
                var hr = component.replacingOccurrences(of: " ", with: "")
                hr = hr.replacingOccurrences(of: "hr", with: "")
                seconds = seconds + (Double(hr)! * 60.0 * 60.0)
            }
            
        }
        return seconds
    }
    
    
    func getFormattedStringForPlayerForSeconds(seconds: Double) -> String {
        
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        
        let formattedString = formatter.string(from: TimeInterval(seconds))!
        return formattedString
        
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    
    func getFileSizeFromURLInMB(url: URL?) -> Double {
        if let sourceUrl = url {
            let asset = AVURLAsset(url: sourceUrl, options: nil)
            let assetSizeInBytes = asset.fileSize ?? 0
            let assetSizeInMB = Double(Double(assetSizeInBytes)/1024.0/1024.0)
            let assetSizeInMBRounded = Double(round(10*Double(assetSizeInMB))/10)
            
            print("assetSizeInMB: \(assetSizeInMB)")
            return assetSizeInMBRounded
        } else {
            return Double()
        }
    }
    
    func getFileSizeFromURLInMBForOggAndOpus(url: URL?) -> Double {
        
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                let assetSizeInMB = size.doubleValue / 1000000.0
                let assetSizeInMBRounded = Double(round(10*Double(assetSizeInMB))/10)
                print("assetSizeInMB: \(assetSizeInMB)")
                return assetSizeInMBRounded
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
        
    }
    
    func getDurationOfFileInSeconds(url: URL?) -> Double {
        if let sourceUrl = url {
            let asset = AVURLAsset(url: sourceUrl, options: nil)
            let audioDuration = asset.duration
            var audioDurationSeconds = CMTimeGetSeconds(audioDuration)
            if audioDurationSeconds == 0.0 {
                audioDurationSeconds = self.getDurationOfFileInSecondsUsingFFMPEG(url: url!)
            }
            return audioDurationSeconds
        } else {
            return Double()
        }
    }
    
    func getDurationOfFileInSecondsUsingFFMPEG(url: URL) -> Double {
        
        let sourceFilePath = url.absoluteString
        let mediaInfo =  MobileFFmpeg.getMediaInformation(sourceFilePath)
        
        let durationInMilliSeconds = mediaInfo?.getDuration() ?? 0
        let durationInSeconds = Double(durationInMilliSeconds) / 1000.0
        return durationInSeconds
        
    }
    
    fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
        guard let input = input else { return nil }
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
    }
    
    fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
        return input.rawValue
    }
    
    func showAlertWithTitle(title: String, message: String, owner: UIViewController) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        owner.present(alert, animated: true, completion: nil)
        
    }
    
    func showDLGPlayerViewController(filePath: URL, owner: UIViewController, previewMode: Bool, songTitle: String) {
        
        let dlgPlayerViewController = DLGPlayerViewController()
        dlgPlayerViewController.url = filePath.absoluteString
        dlgPlayerViewController.songUrl = filePath
        dlgPlayerViewController.songTitle = songTitle
        dlgPlayerViewController.autoplay = true
        dlgPlayerViewController.preventFromScreenLock = true
        dlgPlayerViewController.previewMode = previewMode
        
        dlgPlayerViewController.modalPresentationStyle = .fullScreen
        
        owner.present(dlgPlayerViewController, animated: true) {
            dlgPlayerViewController.close()
            dlgPlayerViewController.open()
        }
    }
    
    func logCallback(_ level: Int32, _ message: String!) {
        
        print("message from FFMPEG: \(message ?? "")")
        
    }

}

extension AVURLAsset {
    
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)
        
        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
    
}

extension String {
    
    func fileName() -> String {
        return NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent ?? ""
    }
    
    func fileExtension() -> String {
        return NSURL(fileURLWithPath: self).pathExtension ?? ""
    }
}
extension String {
    var westernArabicNumeralsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil })
    }
}
