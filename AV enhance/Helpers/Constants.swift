//
//  Constants.swift
//  AV enhance
//
//  Created by umer on 8/3/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
let appLink = "https://apps.apple.com/ug/app/tiktok-make-your-day/id835599320"

class Constants {
    
    
    
    struct TabBarTitels {
        
        static var Audio: String = "Audio"
        static var Video: String = "Video"
        static var Gallery: String = "Gallery"
        static var Settings: String = "Settings"
        
    }
    
    struct ColorCodes {
        
        static var GreyColor = "787979"
        static var BeigeColor = "4eddfe"
        
    }
    
    struct UserDefaults {
        
        static let k_Preview_Before_Importing = "previewBeforeImporting"
        static let k_Pro_Subscription = "isProSubscribed"
        static let k_Default_Audio_Volume = "defaultAudioVolume"
        static let k_Default_Video_Format = "defaultVideoFormat"
        static let k_Default_Audio_Format = "defaultAudioFormat"
        
    }
    
}
