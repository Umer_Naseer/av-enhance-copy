//
//  Enums.swift
//  AV enhance
//
//  Created by umer on 8/3/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

enum MainTabBarIndex: Int {
    case Audio = 0,Video,Gallery,Settings
}

enum MediaType: Int {
    case Audio,Video
}

enum SortBy: Int {
    case Date = 0,Name,Size,Duration
}

enum UIUserInterfaceIdiom : Int {
    case Unspecified = 0,Phone,Pad
}
