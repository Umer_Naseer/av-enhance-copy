//
//  ImportUtility.swift
//  AV enhance
//
//  Created by Apple on 07/08/2019.
//  Copyright © 2019 shujahasan. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

enum ExportError: Error {
    case unableToCreateExporter
}

protocol ImportUtilityDelegate: class {
    
    func exportFinished(destinationUrl: URL, originalFileName: String?)
    
}

extension FileManager {
    func urls(for directory: FileManager.SearchPathDirectory, skipsHiddenFiles: Bool = true ) -> [URL]? {
        let documentsURL = urls(for: directory, in: .userDomainMask)[0]
        let fileURLs = try? contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        return fileURLs
    }
}

class ImportUtility {
    
    static var sharedInstance = ImportUtility()
    let realm = try! Realm()
    weak var delegate: ImportUtilityDelegate?
    
    // MARK: Audio Import
    
    func deleteFileFromDocumentsDirectory(fileName: String) {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent(fileName)
        
        do {
            try FileManager.default.removeItem(atPath: dataPath.path)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
    }
    
    func getDocumentsDirectoryWithAudioFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Audio")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func copyFileToDocumentsDirectory(sourceFileUrl: URL, fileName: String) -> URL {
        
        let fileNameWithExt = "\(fileName).\(sourceFileUrl.lastPathComponent.components(separatedBy: ".").last!)"
        let destinationUrl = self.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
        let copied = self.secureCopyItem(sourceUrl: sourceFileUrl, to: destinationUrl)
        
        print("sourceFileUrl: \(sourceFileUrl)")
        print("destinationUrl: \(destinationUrl)")
        
        if(copied) {
            return destinationUrl
        }
        else {
            return sourceFileUrl
        }
        
    }
    
    func secureCopyItem(sourceUrl: URL, to destinationUrl: URL) -> Bool {
        do {
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                try FileManager.default.removeItem(at: destinationUrl)
            }
            try FileManager.default.copyItem(at: sourceUrl, to: destinationUrl)
        } catch (let error) {
            print("Cannot copy item at \(sourceUrl) to \(destinationUrl): \(error)")
            return false
        }
        return true
        
    }
    
    func export(_ assetURL: URL, fileName: String, originalFileName: String, completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        
        let asset = AVURLAsset(url: assetURL)
        
        var exportPreset = AVAssetExportPresetPassthrough
        if assetURL.absoluteString.contains(".mp3") {
            exportPreset = AVAssetExportPresetAppleM4A
        }
        
        guard let exporter = AVAssetExportSession(asset: asset, presetName: exportPreset) else {
            completionHandler(nil, ExportError.unableToCreateExporter)
            return
        }
        
        let destinationUrl = self.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileName).appendingPathExtension("m4a")
        
        exporter.outputURL = destinationUrl
        exporter.outputFileType = AVFileType.m4a//AVFileType(rawValue: "com.apple.m4a-audio")
        
        exporter.exportAsynchronously {
            if exporter.status == .completed {
                completionHandler(destinationUrl, nil)
            } else {
                completionHandler(nil, exporter.error)
            }
        }
        
    }
    
    func exportIpodMedia(assetURL: URL, fileName: String, originalFileName: String) {
        
        export(assetURL, fileName: fileName, originalFileName: originalFileName) { fileURL, error in
            guard let fileURL = fileURL, error == nil else {
                print("export failed: \(error)")
                return
            }
            
            // use fileURL of temporary file here
            self.delegate?.exportFinished(destinationUrl: fileURL, originalFileName: originalFileName)
        }
        
    }
    
    func deleteItemAt(sourceUrl: URL) {
        
        do {
            if FileManager.default.fileExists(atPath: sourceUrl.path) {
                try FileManager.default.removeItem(at: sourceUrl)
            }
            
        }
        catch (let error) {
            print("Cannot delete item at \(sourceUrl): \(error)")
        }
        
    }
    
    // MARK: Audio Import End
    
    // MARK: Video Import Start
    
    func getDocumentsDirectoryWithVideoFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Video")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func copyFileToDocumentsDirectoryForVideo(sourceFileUrl: URL, fileName: String) -> URL {
        
        let fileNameWithExt = "\(fileName).\(sourceFileUrl.lastPathComponent.components(separatedBy: ".").last!)"
        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
        let copied = self.secureCopyItem(sourceUrl: sourceFileUrl, to: destinationUrl)
        
        print("sourceFileUrl: \(sourceFileUrl)")
        print("destinationUrl: \(destinationUrl)")
        
        if(copied) {
            return destinationUrl
        }
        else {
            return sourceFileUrl
        }
        
    }
    
    func exportVideo(_ assetURL: URL, fileName: String, originalFileName: String, completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        
        let asset = AVURLAsset(url: assetURL)
        
        let exportPreset = AVAssetExportPreset1280x720
        
        guard let exporter = AVAssetExportSession(asset: asset, presetName: exportPreset) else {
            completionHandler(nil, ExportError.unableToCreateExporter)
            return
        }
        
        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileName).appendingPathExtension("mp4")
        
        exporter.outputURL = destinationUrl
        exporter.outputFileType = AVFileType.mp4//AVFileType(rawValue: "com.apple.m4a-audio")
        
        exporter.exportAsynchronously {
            if exporter.status == .completed {
                completionHandler(destinationUrl, nil)
            } else {
                completionHandler(nil, exporter.error)
            }
        }
        
    }
    
    func exportVideoMedia(assetURL: URL, fileName: String, originalFileName: String) {
        
//        let videoData = try? Data(contentsOf: assetURL)
//        let videoData = NSData(contentsOf: assetURL)
        
//        let destinationUrl = self.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileName).appendingPathExtension("mp4")
//        try! videoData?.write(to: destinationUrl, options: [])
        
        exportVideo(assetURL, fileName: fileName, originalFileName: originalFileName) { fileURL, error in
            guard let fileURL = fileURL, error == nil else {
                print("export failed: \(error)")
                return
            }

            // use fileURL of temporary file here
            self.delegate?.exportFinished(destinationUrl: fileURL, originalFileName: originalFileName)
        }
        
    }
    
    // MARK: Video Import End
    
    // MARK: Subtitle Import Start
    
    func getDocumentsDirectoryWithSubtitleFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("Subtitle")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func copySubtitleFileToDocumentsDirectory(sourceFileUrl: URL, fileName: String) -> URL {
        
        let fileNameWithExt = "\(fileName)"
        let destinationUrl = self.getDocumentsDirectoryWithSubtitleFolder().appendingPathComponent(fileNameWithExt)
        let copied = self.secureCopyItem(sourceUrl: sourceFileUrl, to: destinationUrl)
        
        print("sourceFileUrl: \(sourceFileUrl)")
        print("destinationUrl: \(destinationUrl)")
        
        if(copied) {
            return destinationUrl
        }
        else {
            return sourceFileUrl
        }
        
    }
    
    // MARK: Subtitle Import End
    
    // MARK: Background Audio Import Start
    
    func getDocumentsDirectoryWithBackgroundAudioFolder() -> URL {
        
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent("BackgroundAudio")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        return dataPath
        
    }
    
    func copyBackgroundAudioFileToDocumentsDirectory(sourceFileUrl: URL, fileName: String) -> URL {
        
        let fileNameWithExt = "\(fileName)"
        let destinationUrl = self.getDocumentsDirectoryWithBackgroundAudioFolder().appendingPathComponent(fileNameWithExt)
        let copied = self.secureCopyItem(sourceUrl: sourceFileUrl, to: destinationUrl)
        
        print("sourceFileUrl: \(sourceFileUrl)")
        print("destinationUrl: \(destinationUrl)")
        
        if(copied) {
            return destinationUrl
        }
        else {
            return sourceFileUrl
        }
        
    }
    
    func exportBackgroundAudio(_ assetURL: URL, fileName: String, originalFileName: String, completionHandler: @escaping (_ fileURL: URL?, _ error: Error?) -> ()) {
        
        let asset = AVURLAsset(url: assetURL)
        
        
        
        print("assetURL.absoluteString = \(assetURL.absoluteString)")
        
        var exportPreset = AVAssetExportPresetPassthrough
        if assetURL.absoluteString.contains(".mp3") {
            exportPreset = AVAssetExportPresetAppleM4A
        }
        
        guard let exporter = AVAssetExportSession(asset: asset, presetName: exportPreset) else {
            completionHandler(nil, ExportError.unableToCreateExporter)
            return
        }
        
        let destinationUrl = self.getDocumentsDirectoryWithBackgroundAudioFolder().appendingPathComponent(fileName).appendingPathExtension("m4a")
        
        exporter.outputURL = destinationUrl
        exporter.outputFileType = AVFileType.m4a//AVFileType(rawValue: "com.apple.m4a-audio")
        
        exporter.exportAsynchronously {
            if exporter.status == .completed {
                completionHandler(destinationUrl, nil)
            } else {
                completionHandler(nil, exporter.error)
            }
        }
        
    }
    
    func exportBackgroundAudioMedia(assetURL: URL, fileName: String, originalFileName: String) {
        
        exportBackgroundAudio(assetURL, fileName: fileName, originalFileName: originalFileName) { fileURL, error in
            guard let fileURL = fileURL, error == nil else {
                print("export failed: \(error.debugDescription)")
                return
            }
            
            // use fileURL of temporary file here
            self.delegate?.exportFinished(destinationUrl: fileURL, originalFileName: originalFileName)
        }
        
    }
    
    // MARK: Background Audio Import End
    
    // MARK: Audio Database Methods
    
    func getIDForNewAudioModel() -> Int {
        
        var maxID = 0
        if let id =  realm.objects(AudioModel.self).max(ofProperty: "id") as Int? {
            maxID = id
        }
        return maxID+1
        
    }
    
    func addAudioModelToDatabase(audioModel: AudioModel) {
        
        try! realm.write {
            realm.add(audioModel)
        }
        
    }
    
    func getAllAudioModelsFromDatabase() -> Results<AudioModel> {
        
        let audioModels = realm.objects(AudioModel.self)
        return audioModels
        
    }
    
    func getAudioModelForID(id: Int) -> AudioModel {
        
        let audioModel = realm.objects(AudioModel.self).filter("id == \(id)").first ?? AudioModel()
        return audioModel
        
    }
    
    func updateNameForAudioModelForID(id: Int, name: String) {
        
        let audioModel = realm.objects(AudioModel.self).filter("id == \(id)").first ?? AudioModel()
        try! realm.write {
            audioModel.name = name
            audioModel.fullName = name
        }
        
    }
    
    func updateNoteForAudioModelForID(id: Int, note: String) {
        
        let audioModel = realm.objects(AudioModel.self).filter("id == \(id)").first ?? AudioModel()
        try! realm.write {
            audioModel.note = note
        }
        
    }
    
    func deleteAudioModelForID(id: Int) {
        
        if let audioModel = realm.objects(AudioModel.self).filter("id == \(id)").first {
            let filePath = audioModel.filePath
            let fileUrl = NSURL(string: filePath) ?? NSURL()
            // delete file first
            self.deleteItemAt(sourceUrl: fileUrl as URL)
            
            try! realm.write {
                realm.delete(audioModel)
            }
        }
        
    }
    
    // MARK: Audio Database Methods End
    
    // MARK: Video Database Methods
    
    func getIDForNewVideoModel() -> Int {
        
        var maxID = 0
        if let id =  realm.objects(VideoModel.self).max(ofProperty: "id") as Int? {
            maxID = id
        }
        return maxID+1
        
    }
    
    func addVideoModelToDatabase(videoModel: VideoModel) {
        
        try! realm.write {
            realm.add(videoModel)
        }
        
    }
    
    func getAllVideoModelsFromDatabase() -> Results<VideoModel> {
        
        let videoModels = realm.objects(VideoModel.self)
        return videoModels
        
    }
    
    func getVideoModelForID(id: Int) -> VideoModel {
        
        let videoModel = realm.objects(VideoModel.self).filter("id == \(id)").first ?? VideoModel()
        return videoModel
        
    }
    
    func updateNameForVideoModelForID(id: Int, name: String) {
        
        if let videoModel = realm.objects(VideoModel.self).filter("id == \(id)").first {
            try! realm.write {
                videoModel.name = name
                videoModel.fullName = name
            }
        }
    }
    
    func updateNoteForVideoModelForID(id: Int, note: String) {
        
        let videoModel = realm.objects(VideoModel.self).filter("id == \(id)").first ?? VideoModel()
        try! realm.write {
            videoModel.note = note
        }
        
    }
    
    func deleteVideoModelForID(id: Int) {
        
        if let videoModel = realm.objects(VideoModel.self).filter("id == \(id)").first {
            let filePath = videoModel.filePath
            let fileUrl = NSURL(string: filePath) ?? NSURL()
            // delete file first
            self.deleteItemAt(sourceUrl: fileUrl as URL)
            
            try! realm.write {
                realm.delete(videoModel)
            }
        }
        
    }
    
    // MARK: Video Database Methods End

}
