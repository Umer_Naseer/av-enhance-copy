//
//  MediaItems.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit

class MediaItems {
    
    var id = Int()
    var videoName = String()
    var fullName = String()
    var videoType = String()
    var videoLength = String()
    var videoSize = Data()
    var videoDesc = String()
    var date = Date()
    var fileType = String()
    var soundLevel = String()
    var fileTypeToConvert = String()
    var isAudio = Bool()

}
