//
//  AudioModel.swift
//  AV enhance
//
//  Created by Apple on 07/08/2019.
//  Copyright © 2019 shujahasan. All rights reserved.
//

import UIKit
import RealmSwift

class AudioModel: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var fullName = ""

    @objc dynamic var size = 0.0
    @objc dynamic var duration = 0.0
    @objc dynamic var filePath = ""
    @objc dynamic var fileType = ""
    @objc dynamic var note = ""
    @objc dynamic var dateCreated = Date()
    
    override static func primaryKey() -> String? {
        return "id"
    }

}
