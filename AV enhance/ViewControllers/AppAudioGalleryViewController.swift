//
//  AppAudioGalleryViewController.swift
//  AV enhance
//
//  Created by AIR on 05/08/2020.
//  Copyright © 2020 shujahasan. All rights reserved.
//

import UIKit

protocol AppAudioGalleryViewControllerDelegate: NSObjectProtocol {
    func didSelectAudio(withMediaItems mediaItems : MediaItems)
}
class AppAudioGalleryViewController: UIViewController {
    
    @IBOutlet weak var mainTableView: UITableView!

    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var emptyDataSourceView: UIView!

    weak var delegate: AppAudioGalleryViewControllerDelegate?

    // Class Variables
    var searchTerm = String()
    
    var selectedMediaItem = MediaItems()
    var mediaItemsArray = [MediaItems]()

    var selectedSortType = SortBy.Name
    var filteredDataSource: [MediaItems] {
        


        
        let audioGalleryArray = self.sortArrayAccordingToSortType(array: self.mediaItemsArray)
        print("COUNT audioGalleryArray = \(audioGalleryArray.count)")

        return  audioGalleryArray

        
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapDataSource()
        self.setupUIViews()
        // Do any additional setup after loading the view.
        self.mainTableView.delegate = self
        self.mainTableView.dataSource = self
        
        if filteredDataSource.count == 0 {
            self.emptyDataSourceView.isHidden = false
        }

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func cancelAction(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sortArrayAccordingToSortType(array : [MediaItems]) -> [MediaItems]
    {
        var ascendingOrder = true
        if self.selectedSortType == .Date {
            ascendingOrder = false
        }
        
        let sortedArray = self.sortArray(usingArray: array, isAscending: ascendingOrder, sortingType: self.selectedSortType)
        
        return sortedArray
    }
    
    func sortArray(usingArray arrayToSort:[MediaItems], isAscending : Bool, sortingType : SortBy) -> [MediaItems] {
        var array = arrayToSort
        
        switch sortingType {
        case .Name:
            array = array.sorted(by: { $0.videoName.compare($1.videoName, options: .caseInsensitive, range: nil, locale: nil) == .orderedAscending })//array.sorted(by: { $0.videoName < $1.videoName })
            
        case .Date:
            array = array.sorted(by: { $0.date < $1.date })
            
        case .Duration:
            array = array.sorted(by: { CommonClass.sharedInstance.getSecondsFromString(string: $0.videoLength) < CommonClass.sharedInstance.getSecondsFromString(string: $1.videoLength) })

        case .Size:
            array = array.sorted(by: { ByteCountFormatter().string(fromByteCount: Int64($0.videoSize.count)) < ByteCountFormatter().string(fromByteCount: Int64($1.videoSize.count)) })
        }
        
        if isAscending == true {
            return array
        }
        else{
            return array.reversed()
        }
    }
    
    
    
    func sortAudioArrayAccordingToSortType(array : [MediaItems]) -> [MediaItems]
    {
        var ascendingOrder = true
        if self.selectedSortType == .Date {
            ascendingOrder = false
        }
        
        let sortedArray = self.sortAudioArray(usingArray: array, isAscending: ascendingOrder, sortingType: self.selectedSortType)
        
        return sortedArray
    }
    
    func sortAudioArray(usingArray arrayToSort:[MediaItems], isAscending : Bool, sortingType : SortBy) -> [MediaItems] {
        var array = arrayToSort
        
        switch sortingType {
        case .Name:
            array = array.sorted(by: { $0.videoName.compare($1.videoName, options: .caseInsensitive, range: nil, locale: nil) == .orderedAscending })//array.sorted(by: { $0.videoName < $1.videoName })
            
        case .Date:
            array = array.sorted(by: { $0.date < $1.date })
            
        case .Duration:
            array = array.sorted(by: { CommonClass.sharedInstance.getSecondsFromString(string: $0.videoLength) < CommonClass.sharedInstance.getSecondsFromString(string: $1.videoLength) })

        case .Size:
            array = array.sorted(by: { ByteCountFormatter().string(fromByteCount: Int64($0.videoSize.count)) < ByteCountFormatter().string(fromByteCount: Int64($1.videoSize.count)) })
        }
        
        if isAscending == true {
            return array
        }
        else{
            return array.reversed()
        }
    }
    
    
    
    
    
    func mapDataSource() {
        
        self.mediaItemsArray.removeAll()
        let audioExportModels = ExportUtility.sharedInstance.getAllAudioExportModelsFromDatabase()
        
        for audioExportModel in audioExportModels {
            let media = MediaItems()
            media.id = audioExportModel.id
            media.videoName = audioExportModel.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: audioExportModel.duration)
            media.videoDesc = "\(audioExportModel.size) mb"
            media.isAudio = true
            
            let fileExtension = audioExportModel.fileType
            media.fileType = fileExtension
            media.date = audioExportModel.dateCreated
            
            self.mediaItemsArray.append(media)
        }
        
        let audioModels = ImportUtility.sharedInstance.getAllAudioModelsFromDatabase()
        
        for audioModel in audioModels {
            
            let media = MediaItems()
            media.id = audioModel.id
            media.videoName = audioModel.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: audioModel.duration)
            media.videoDesc = "\(audioModel.size) mb"
            media.isAudio = true
            
            let fileExtension = audioModel.fileType.fileExtension()
            media.fileType = fileExtension
            media.date = audioModel.dateCreated
            
            self.mediaItemsArray.append(media)
            
        }
        
        
        
        
    }
    
    func setupUIViews() {
        self.mainTableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.mainTableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.mainTableView.estimatedRowHeight = 58

    }
}

extension AppAudioGalleryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filteredDataSource.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > self.filteredDataSource.count - 1 {return}
        
        
        let model = self.filteredDataSource[indexPath.row]
        
    
        self.delegate?.didSelectAudio(withMediaItems: model)
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        if indexPath.row > self.filteredDataSource.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
            return cell
        }
        
        let model = self.filteredDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model,searchTerm: self.searchTerm)
        cell.moreImageView.isHidden = true
        
        return cell
        
    }
}
