//
//  AudioPlayerViewController.swift
//  AV enhance
//
//  Created by Apple on 07/08/2019.
//  Copyright © 2019 shujahasan. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioPlayerViewControllerDelegate: class {
    func importAudioPressed(songTitle: String, songUrl: URL)
}

class AudioPlayerViewController: UIViewController {
    
    @IBOutlet weak var importButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var replayButton: UIButton!
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    
    var songPlayer: AVAudioPlayer!
    var hasBeenPaused = false
    var songTitle = ""
    var songUrl: URL?
    var previewMode = false
    
    weak var delegate: AudioPlayerViewControllerDelegate?

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupViews()
        prepareSongAndSession()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.songPlayer.stop()
    }
    
    func setupViews() {
        
        if(previewMode) {
            self.importButton.isHidden = true
        }
        
    }
    
    func prepareSongAndSession() {
        if let url = self.songUrl {
            do {
                //7 - Insert the song from our Bundle into our AVAudioPlayer
                songPlayer = try AVAudioPlayer(contentsOf: url)
                //8 - Prepare the song to be played
                songPlayer.prepareToPlay()
                
                // set duration in label
                let duration = self.songPlayer.duration
                self.endLabel.text = CommonClass.sharedInstance.getFormattedStringForPlayerForSeconds(seconds: duration)
                
                //9 - Create an audio session
                let audioSession = AVAudioSession.sharedInstance()
                do {
                    //10 - Set our session category to playback music
                    
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: AVAudioSession.Mode.default, options: [])
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    //                try audioSession.setCategory(AVAudioSession.Category.playback)
                    
                    self.play()
                    //11 -
                } catch let sessionError {
                    
                    print("sessionError: \(sessionError)")
                }
                //12 -
            } catch let songPlayerError {
                print("songPlayerError: \(songPlayerError)")
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        if songPlayer.isPlaying {
            songPlayer.pause()
            hasBeenPaused = true
        }
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func importButtonPressed(_ sender: Any) {
        
        if songPlayer.isPlaying {
            songPlayer.pause()
            hasBeenPaused = true
        }
        if let url = self.songUrl {
            delegate?.importAudioPressed(songTitle: self.songTitle, songUrl: url)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func play() {
        
        if songPlayer.isPlaying {
            songPlayer.pause()
        } else {
            songPlayer.play()
        }
        
        let updater = CADisplayLink(target: self, selector: #selector(self.musicProgress))
        updater.preferredFramesPerSecond = 60
        updater.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
        
    }
    
    @IBAction func pause(_ sender: Any) {
        
        if songPlayer.isPlaying {
            songPlayer.pause()
            hasBeenPaused = true
        } else {
            hasBeenPaused = false
        }
        
    }
    
    @IBAction func restart(_ sender: Any) {
        
        if songPlayer.isPlaying || hasBeenPaused {
            songPlayer.stop()
            songPlayer.currentTime = 0
            
            songPlayer.play()
        } else  {
            songPlayer.play()
        }
        
    }
    
    @objc func musicProgress()  {
        
        let currentTime = self.songPlayer.currentTime
        let (h,m,s) = CommonClass.sharedInstance.secondsToHoursMinutesSeconds(seconds: Int(currentTime))
        self.startLabel.text = "\(h):\(m):\(s)"
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
