//
//  AudioViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import RealmSwift
import SVProgressHUD

class AudioViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    
    // Class Variables
    var searchTerm = String()
    var selectionMode = false
    
    var selectedMediaItem = MediaItems()
    var selectedMediaItemsArray: [MediaItems] = []
    
    var mediaItemsArray = [MediaItems]()
    var selectedSortType = SortBy.Name
    var filteredDataSource: [MediaItems] {
        
        if(searchTerm == "") {
            //return mediaItemsArray
            return self.sortArrayAccordingToSortType(array: self.mediaItemsArray)
        } else {
            let array =  mediaItemsArray.filter({$0.videoName.lowercased().contains(self.searchTerm.lowercased())})
            return self.sortArrayAccordingToSortType(array: array)
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.mapDataSource()
        self.setupUIViews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        
        self.selectedMediaItemsArray.removeAll()
        self.tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.importAudiosFromItunesLibrary()
        
    }
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
        
    }()
    
}

// Private Methods
extension AudioViewController {
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.tableView.estimatedRowHeight = 58
        
        self.tableView.addSubview(self.refreshControl)
        
        self.setSortByButtonTitle()
        
    }
    
    func mapDataSource() {
        
        self.mediaItemsArray.removeAll()
        let audioModels = ImportUtility.sharedInstance.getAllAudioModelsFromDatabase()
        
        for audioModel in audioModels {
            
            let media = MediaItems()
            media.id = audioModel.id
            media.videoName = audioModel.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: audioModel.duration)
            media.videoDesc = "\(audioModel.size) mb"
            media.isAudio = true
            
            let fileExtension = audioModel.fileType.fileExtension()
            media.fileType = fileExtension
            media.date = audioModel.dateCreated
            media.fullName = audioModel.fullName
            self.mediaItemsArray.append(media)
            
        }
        
        updateUI()
        
    }
    
    func setSortByButtonTitle() {
        
        switch self.selectedSortType {
        case .Name:
            self.sortByLabel.text = "Sort by Name"
        case .Date:
            self.sortByLabel.text = "Sort by Date"
        case .Duration:
            self.sortByLabel.text = "Sort by Duration"
        case .Size:
            self.sortByLabel.text = "Sort by Size"
        }
        
    }
    
    func updateUI() {
        
        if(self.mediaItemsArray.count == 0) {
            self.selectButton.isHidden = true
        }
        else {
            self.selectButton.isHidden = false
        }
        
    }
    
    @objc func handleRefresh() {
        
        self.importAudiosFromItunesLibrary()
        
    }
    
    func openItemSheet(item: MediaItems, mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Boost Volume",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.openConverterVC()
        }))
        sheet.addAction(UIAlertAction(title: "Preview",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.previewAudioPressed(id: self.selectedMediaItem.id)
        }))
        sheet.addAction(UIAlertAction(title: "Rename",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.renameAudioPressed(id: self.selectedMediaItem.id)
        }))
        sheet.addAction(UIAlertAction(title: "Delete",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        self.deleteAudioPressed(id: self.selectedMediaItem.id)
        }))
        
        if(self.selectedMediaItemsArray.count > 1) {
            sheet.actions[2].isEnabled = false
            sheet.actions[3].isEnabled = false
            sheet.actions[4].isEnabled = false
        }
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.audioButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func openConverterVC() {
        let vc = ConverterViewController(nibName: "ConverterViewController", bundle: nil)
        vc.selectedMediaType = MediaType.Audio
        if(self.selectedMediaItemsArray.count > 0) {
            vc.mediaItemsArray = self.selectedMediaItemsArray
        }
        else {
            vc.mediaItemsArray = [self.selectedMediaItem]
        }
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func sortArrayAccordingToSortType(array : [MediaItems]) -> [MediaItems]
    {
        var ascendingOrder = true
        if self.selectedSortType == .Date {
            ascendingOrder = false
        }
        
        let sortedArray = self.sortArray(usingArray: array, isAscending: ascendingOrder, sortingType: self.selectedSortType)
        
        return sortedArray
    }
    
    func sortArray(usingArray arrayToSort:[MediaItems], isAscending : Bool, sortingType : SortBy) -> [MediaItems] {
        var array = arrayToSort
        
        switch sortingType {
        case .Name:
            array = array.sorted(by: { $0.videoName.compare($1.videoName, options: .caseInsensitive, range: nil, locale: nil) == .orderedAscending })//array.sorted(by: { $0.videoName < $1.videoName })
            
        case .Date:
            array = array.sorted(by: { $0.date < $1.date })
            
        case .Duration:
            array = array.sorted(by: { CommonClass.sharedInstance.getSecondsFromString(string: $0.videoLength) < CommonClass.sharedInstance.getSecondsFromString(string: $1.videoLength) })
        case .Size:
            array = array.sorted(by: { ByteCountFormatter().string(fromByteCount: Int64($0.videoSize.count)) < ByteCountFormatter().string(fromByteCount: Int64($1.videoSize.count)) })
        }
        if isAscending == true {
            return array
        }
        else{
            return array.reversed()
        }
    }
    
    func importAudiosFromItunesLibrary() {
        if let urls = FileManager.default.urls(for: .documentDirectory) {
            print("urls")
            var urlsToDelete = [URL]()
            for each in urls {
                if each.absoluteString.contains(".mp3") || each.absoluteString.contains(".m4a") || each.absoluteString.contains(".wma") || each.absoluteString.contains(".adx") || each.absoluteString.contains(".flac") || each.absoluteString.contains(".aac") ||
                    each.absoluteString.contains(".wav") ||
                    each.absoluteString.contains(".opus") ||
                    each.absoluteString.contains(".caf") ||
                    each.absoluteString.contains(".aiff") ||
                    each.absoluteString.contains(".ogg") ||
                    each.absoluteString.contains(".aif") ||
                    each.absoluteString.contains(".ac3") ||
                    each.absoluteString.contains(".au") ||
                    each.absoluteString.contains(".mp2") ||
                    each.absoluteString.contains(".amr") ||
                    each.absoluteString.contains(".awb") ||
                    each.absoluteString.contains(".mmf"){
                    print(each as Any)
                    urlsToDelete.append(each)
                    self.importAudioPressed(songTitle: each.lastPathComponent.components(separatedBy: ".").first ?? "", songUrl: each)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.refreshControl.endRefreshing()
                for each in urlsToDelete {
                    ImportUtility.sharedInstance.deleteFileFromDocumentsDirectory(fileName: each.lastPathComponent)
                }
            })
        }
    }
}

// MARK: Extension for IBActions
extension AudioViewController {
    
    @IBAction func didTapAdd(sender: UIButton) {
        
        let sheet = UIAlertController.init(title: "Import Audio", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Music Library",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.openMusicLibrary()
                                        
        }))
        sheet.addAction(UIAlertAction(title: "Files App (ICloud)",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.openFilesApp()
                                        
        }))
        sheet.addAction(UIAlertAction(title: "Itunes File Sharing",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        let vc = WebViewViewController(nibName: "WebViewViewController", bundle: nil)
                                        vc.modalPresentationStyle = .fullScreen
                                        vc.selectedScreenType = SettingsRowType.ItunesFileSharing
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        
        }))
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: sender, owner: self)
        
    }
    
    @IBAction func didTapSelect(sender: UIButton) {
        
        if(selectionMode) {
            selectionMode = false
            selectButton.setTitle("Select", for: .normal)
        }
        else {
            selectionMode = true
            selectButton.setTitle("Done", for: .normal)
        }
        self.tableView.reloadData()
        
    }
    
    @IBAction func didTapSortBy(sender: UIButton) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Date",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Date
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Name",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Name
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Duration",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Duration
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: sender, owner: self)
        
    }
    
    func openMusicLibrary() {
        
        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes: MPMediaType.music)
        mediaPicker.delegate = self
        mediaPicker.allowsPickingMultipleItems = false
        self.present(mediaPicker, animated: true, completion: nil)
        
    }
    
    
    
    func openFilesApp() {
        
        let controller = UIDocumentPickerViewController(
            documentTypes: ["public.audio"], // choose your desired documents the user is allowed to select
            in: .import // choose your desired UIDocumentPickerMode
        )
        controller.delegate = self
        if #available(iOS 11.0, *) {
            controller.allowsMultipleSelection = false
        } else {
            // Fallback on earlier versions
        }
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
}

// MARK: Extension for Search
extension AudioViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.searchTerm = ""
            self.tableView.reloadData()
        } else {
            self.searchTerm = searchText.lowercased()
            self.tableView.reloadData()
        }
    }
}

// MARK: Extension for Import
extension AudioViewController: AudioPlayerViewControllerDelegate, ImportUtilityDelegate {
    
    func importAudioPressed(songTitle: String, songUrl: URL) {
        
        ImportUtility.sharedInstance.delegate = self
        SVProgressHUD.show()
        
        var originalFileName = ""
        
        // Song Title
        if(songTitle.count > 0) {
            originalFileName = songTitle
        }
        else {
            originalFileName = songUrl.lastPathComponent.fileName()
        }
        let fullFileName = originalFileName

        let originalFileNameWithoutSpaces = originalFileName.replacingOccurrences(of: " ", with: "-")
        
        let name = "\(originalFileNameWithoutSpaces)-\(CommonClass.sharedInstance.getCurrentDateInString())"
        
        // Get Destination URL
        if((songUrl.absoluteString.range(of: "ipod-library")) != nil) {
            
            ImportUtility.sharedInstance.exportIpodMedia(assetURL: songUrl as URL, fileName: name, originalFileName: originalFileName)
            
        }
        else {
            let destinationUrl = ImportUtility.sharedInstance.copyFileToDocumentsDirectory(sourceFileUrl: songUrl as URL, fileName: name)
            
            // Get duration
            let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
            let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
            
            let audioModel = AudioModel()
            audioModel.id = ImportUtility.sharedInstance.getIDForNewAudioModel()
            audioModel.name = originalFileName.capitalized
            audioModel.size = fileSize
            audioModel.duration = duration
            audioModel.filePath = "\(destinationUrl.lastPathComponent)"
            
            audioModel.fileType = destinationUrl.lastPathComponent
            audioModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            audioModel.fullName = fullFileName
            
            ImportUtility.sharedInstance.addAudioModelToDatabase(audioModel: audioModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
    }
    
    func exportFinished(destinationUrl: URL, originalFileName: String?) {
        
        // Get duration
        let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
        let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
        
        DispatchQueue.main.async {
            
            let audioModel = AudioModel()
            audioModel.id = ImportUtility.sharedInstance.getIDForNewAudioModel()
            audioModel.name = originalFileName?.capitalized ?? ""
            audioModel.size = fileSize
            audioModel.duration = duration
            audioModel.filePath = "\(destinationUrl.lastPathComponent ?? "m4a")"
            
            audioModel.fileType = destinationUrl.lastPathComponent ?? "m4a"
            audioModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            
            ImportUtility.sharedInstance.addAudioModelToDatabase(audioModel: audioModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
    }
}

// MARK: Extension for Audio Player Delegate
extension AudioViewController: DLGPlayerViewControllerDelegate {
    
    func importAudioPressed(_ songTitle: String!, songUrl: URL!) {
        
        ImportUtility.sharedInstance.delegate = self
        SVProgressHUD.show()
        
        var originalFileName = ""
        
        let name = "Audio-\(CommonClass.sharedInstance.getCurrentDateInString())"
        
        // Song Title
        if(songTitle.count > 0) {
            originalFileName = songTitle
        }
        else {
            originalFileName = songUrl.lastPathComponent.fileName()
        }
        
        // Get Destination URL
        if((songUrl.absoluteString.range(of: "ipod-library")) != nil) {
            
            ImportUtility.sharedInstance.exportIpodMedia(assetURL: songUrl as URL, fileName: name, originalFileName: originalFileName)
            
        }
        else {
            let destinationUrl = ImportUtility.sharedInstance.copyFileToDocumentsDirectory(sourceFileUrl: songUrl as URL, fileName: name)
            
            // Get duration
            let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
            let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
            
            let audioModel = AudioModel()
            audioModel.id = ImportUtility.sharedInstance.getIDForNewAudioModel()
            audioModel.name = originalFileName.capitalized
            audioModel.size = fileSize
            audioModel.duration = duration
            audioModel.filePath = "\(destinationUrl.lastPathComponent)"
            
            audioModel.fileType = destinationUrl.lastPathComponent
            audioModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            
            ImportUtility.sharedInstance.addAudioModelToDatabase(audioModel: audioModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
        
    }
    
}

// MARK: Extension for UI Actions
extension AudioViewController {
    
    func reloadDataSource() {
        
        // Refresh data source
        self.mapDataSource()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    func deleteAudioPressed(id: Int) {
        
        let alertController = UIAlertController(title: "Are you sure you want to delete this Audio?", message: "", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { alert -> Void in
            ImportUtility.sharedInstance.deleteAudioModelForID(id: id)
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func renameAudioPressed(id: Int) {
        
        let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: id)
        
        let alertController = UIAlertController(title: "Rename", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = audioModel.name
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            ImportUtility.sharedInstance.updateNameForAudioModelForID(id: id, name: firstTextField.text ?? "")
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func previewAudioPressed(id: Int) {
        
        let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: id)
        let songTitle = audioModel.name
        let filePath = audioModel.filePath
        var songUrl = URL.init(string: "")
        
        if((filePath.range(of: "ipod-library")) != nil) {
            // its coming from Music App
            songUrl = URL.init(fileURLWithPath: filePath)//NSURL(string: filePath) ?? NSURL()
        }
        else {
            // it's from Files App
            let fileNameWithExt = filePath
            let destinationUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
            songUrl = destinationUrl
        }
        
        //        print("songUrl: \(songUrl)")
        
        // Open Preview Controller
        if let url = songUrl {
            self.showAudioPlayerViewController(songTitle: songTitle, songUrl: url, previewMode: true)
        }
        
    }
    
}

// MARK: Extension for Table View
extension AudioViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filteredDataSource.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row > self.filteredDataSource.count - 1 {return}
        
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        
        let model = self.filteredDataSource[indexPath.row]
        self.selectedMediaItem = model
        
        if selectionMode {
            if self.checkIfMediaItemIsAlreadySelected(mediaItems: model) {
                self.removeFromSelectedArray(mediaItems: model)
            }
            else {
                selectedMediaItemsArray.append(model)
            }
            tableView.reloadData()
        } else {
            self.openItemSheet(item: self.filteredDataSource[indexPath.row], mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        }
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        if indexPath.row > self.filteredDataSource.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
            return cell
        }
        
        let model = self.filteredDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model,searchTerm: self.searchTerm)
        
        // Selection
        if(checkIfMediaItemIsAlreadySelected(mediaItems: model)) {
            cell.selectedImageView?.isHidden = false
        }
        else {
            cell.selectedImageView?.isHidden = true
        }
        
        return cell
        
    }
    
}

// MARK: Extension for File Picker
extension AudioViewController: MPMediaPickerControllerDelegate {
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        guard let mediaItem = mediaItemCollection.items.first else {
            NSLog("No item selected.")
            return
        }
        
        // Get the song URL
        var songTitle = mediaItem.value(forProperty: MPMediaItemPropertyTitle) as? String ?? ""
        if let title = mediaItem.title {
            songTitle = title
        }
        
        if let url = mediaItem.assetURL {
            self.dismiss(animated: true) {
                if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
                    
                    if(previewBeforeImporting == "true") {
                        // Open Preview Controller
                        self.showAudioPlayerViewController(songTitle: songTitle, songUrl: url, previewMode: false)
                    }
                    else {
                        // Import directly
                        self.importAudioPressed(songTitle: songTitle, songUrl: url)
                    }
                    
                }
                else {
                    // Import directly
                    self.importAudioPressed(songTitle: songTitle, songUrl: url)
                }
            }
        }
        //        let songUrl = mediaItem.value(forProperty: MPMediaItemPropertyAssetURL) as! NSURL
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

// MARK: Extension for Document Picker
extension AudioViewController: UIDocumentPickerDelegate {
    
    @available(iOS 11.0, *)
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        let songUrl = urls[0]
        
        if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
            
            if(previewBeforeImporting == "true") {
                // Open Preview Controller
                self.showAudioPlayerViewController(songTitle: "", songUrl: songUrl, previewMode: false)
            }
            else {
                // Import directly
                self.importAudioPressed(songTitle: "", songUrl: songUrl)
            }
            
        }
        else {
            // Import directly
            self.importAudioPressed(songTitle: "", songUrl: songUrl)
        }
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let songUrl = url
        
        if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
            
            if(previewBeforeImporting == "true") {
                // Open Preview Controller
                self.showAudioPlayerViewController(songTitle: "", songUrl: songUrl, previewMode: false)
            }
            else {
                // Import directly
                self.importAudioPressed(songTitle: "", songUrl: songUrl)
            }
            
        }
        else {
            // Import directly
            self.importAudioPressed(songTitle: "", songUrl: songUrl)
        }
        
    }
    
}

// MARK: Extension for Audio Player
extension AudioViewController {
    
    func showAudioPlayerViewController(songTitle: String, songUrl: URL, previewMode: Bool) {
        
        let dlgPlayerViewController = DLGPlayerViewController()
        dlgPlayerViewController.url = songUrl.absoluteString
        dlgPlayerViewController.songUrl = songUrl
        dlgPlayerViewController.songTitle = songTitle
        dlgPlayerViewController.autoplay = true
        dlgPlayerViewController.preventFromScreenLock = true
        dlgPlayerViewController.previewMode = previewMode
        dlgPlayerViewController.delegate = self
        
        dlgPlayerViewController.modalPresentationStyle = .fullScreen
        
        self.present(dlgPlayerViewController, animated: true) {
            dlgPlayerViewController.close()
            dlgPlayerViewController.open()
        }
        
    }
    
}

// MARK: Extension for Selection
extension AudioViewController {
    
    func checkIfMediaItemIsAlreadySelected(mediaItems: MediaItems) -> Bool {
        
        var alreadyPresent = false
        
        if self.selectedMediaItemsArray.contains( where: { $0.id == mediaItems.id } ) == true {
            alreadyPresent = true
        }
        
        return alreadyPresent
        
    }
    
    func removeFromSelectedArray(mediaItems: MediaItems) {
        
        if let index = selectedMediaItemsArray.firstIndex(where: {$0.id == mediaItems.id}) {
            selectedMediaItemsArray.remove(at: index)
        }
        
    }
    
}

extension AudioViewController: ConverterVCDelegate {
    func didConvertedAudio() {
        self.selectedMediaItemsArray = []
        self.tabBarController?.selectedIndex = 2
    }
}
