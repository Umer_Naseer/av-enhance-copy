//
//  ConverterViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import SVProgressHUD
import mobileffmpeg



typealias Dispatch = DispatchQueue

protocol ConverterVCDelegate: NSObjectProtocol {
    func didConvertedAudio()
}

class ConverterViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var convertButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    // Video Controls
    @IBOutlet weak var videoEditContainerView: UIStackView!
    @IBOutlet weak var stabilizeVideoSwitch: UISwitch!
    
    @IBOutlet weak var musicNameLabel: UILabel!
    @IBOutlet weak var addMusicButton: UIButton!
    @IBOutlet weak var removeMusicButton: UIButton!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var addSubtitleButton: UIButton!
    @IBOutlet weak var removeSubtitleButton: UIButton!
    
    
    
    @IBOutlet weak var loadingView: UIView!

    // Class Variables
    
    var selectedMediaType = MediaType.Audio
    var mediaItemsArray = [MediaItems]()
    var selectedMediaItem = MediaItems()
    var convertButtonShouldBeEnabled = false
    
    var ffmpegCommandsArray = NSMutableArray()
    var destinationFilePathsArray = NSMutableArray()
    
    var defaultAudioVolume = "100% (No Change)"
    var defaultVideoFormat = "mov"
    var defaultAudioFormat = "aac"
    weak var delegate: ConverterVCDelegate?
    
    // Video Variables
    
    var stabilizeVideo = false
    var musicURL: URL?
    var subtitleURL: URL?
    var subtitleName = String()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupUIViews()
        setupVideoViews()
        self.setDefaultValues()
        
        SVProgressHUD.setDefaultMaskType(.clear)
        self.loadingView.isHidden = true

//        if let url = Bundle.main.url(forResource: "subtitle", withExtension: "srt") {
//            self.subtitleURL = url
//        }
//
//        do {
//            try FileManager.init().copyfileToUserDocumentDirectory(forResource: "subtitle",
//            ofType: "srt")
//        } catch {
//            print("error")
//        }
//
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        
    }
    
    func setDefaultValues() {
        
        // Default Audio Volume
        if let defaultAudioVolume = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Volume) as? String {
            self.defaultAudioVolume = defaultAudioVolume
        }
        
        // Default Video Format
        if let defaultVideoFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Video_Format) as? String {
            self.defaultVideoFormat = defaultVideoFormat
        }
        
        // Default Audio Format
        if let defaultAudioFormat = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Default_Audio_Format) as? String {
            self.defaultAudioFormat = defaultAudioFormat
        }
        
        for mediaItem in self.mediaItemsArray {
            
            if(mediaItem.isAudio) {
                // Audio
                mediaItem.fileTypeToConvert = self.defaultAudioFormat
            }
            else {
                // Video
                mediaItem.fileTypeToConvert = self.defaultVideoFormat
            }
            mediaItem.soundLevel = self.defaultAudioVolume
            
        }
        
        self.refreshData()
        
    }
    
}

// MARK: Extension for UI Actions
extension ConverterViewController {
    
    func setupVideoViews() {
        
        if self.selectedMediaType == .Video {
            
            // Layouts
            self.musicNameLabel.layer.cornerRadius = self.musicNameLabel.frame.size.height/2
            
            self.addMusicButton.layer.borderColor = UIColor.black.cgColor
            self.addMusicButton.layer.borderWidth = 1.0
            self.addMusicButton.layer.cornerRadius = self.addMusicButton.frame.size.height/2
            
            self.removeMusicButton.layer.cornerRadius = self.removeMusicButton.frame.size.height/2
            
            self.subtitleLabel.layer.cornerRadius = self.subtitleLabel.frame.size.height/2
            
            self.addSubtitleButton.layer.borderColor = UIColor.black.cgColor
            self.addSubtitleButton.layer.borderWidth = 1.0
            self.addSubtitleButton.layer.cornerRadius = self.addSubtitleButton.frame.size.height/2
            
            self.removeSubtitleButton.layer.cornerRadius = self.removeSubtitleButton.frame.size.height/2
            
            // Values
            self.stabilizeVideoSwitch.isOn = false
            
            self.musicNameLabel.text = "..."
            self.subtitleLabel.text = "..."
            
            // Hidden
            self.removeMusicButton.isHidden = true
            self.removeSubtitleButton.isHidden = true
            self.videoEditContainerView.isHidden = false
        }
        
    }
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.tableView.estimatedRowHeight = 58
        
        self.convertButton.layer.cornerRadius = 15.0
        
    }
    
    func openFormatSheet(mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            self.convertButtonShouldBeEnabled = false
        }))
        if self.selectedMediaType == .Audio {
            sheet.addAction(UIAlertAction(title: "aac",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "aac"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "wav",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "wav"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "flac",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "flac"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "opus",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "opus"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "caf",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "caf"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "wma",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "wma"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "aiff",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "aiff"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "mp3",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mp3"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "ogg",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "ogg"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "m4a",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "m4a"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "adx",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "adx"
                                            self.refreshData()
            }))
        } else {
            
            
            sheet.addAction(UIAlertAction(title: "mov",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mov"
                                            self.refreshData()
            }))

            sheet.addAction(UIAlertAction(title: "mp4",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mp4"
                                            self.refreshData()
            }))
            
            sheet.addAction(UIAlertAction(title: "mpg",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mpg"
                                            self.refreshData()
            }))

            sheet.addAction(UIAlertAction(title: "avi",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "avi"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "wmv",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "wmv"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "ogg",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "ogg"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "webm",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "webm"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "mkv (H.264)",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mkv (H.264)"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "mkv (H.265, slow)",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "mkv (H.265, slow)"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "m2ts",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "m2ts"
                                            self.refreshData()
            }))
            sheet.addAction(UIAlertAction(title: "gif",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.selectedMediaItem.fileTypeToConvert = "gif"
                                            self.refreshData()
            }))
        }
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.formatButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func openAudioSheet(mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            self.convertButtonShouldBeEnabled = false
        }))
        sheet.addAction(UIAlertAction(title: "0.0 %(Mute)",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "0.0 %(Mute)"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "0.1 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "0.1 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "0.5 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "0.5 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "10.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "10.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "20.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "20.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "30.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "30.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "40.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "40.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "50.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "50.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "60.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "60.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "70.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "70.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "80.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "80.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "90.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "90.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "100% (No Change)",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "100% (No Change)"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "200.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "200.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "300.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "300.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "400.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "400.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "500.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "500.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "600.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "600.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "700.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "700.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "800.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "800.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "900.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "900.0 %"
                                        self.refreshData()
        }))
        sheet.addAction(UIAlertAction(title: "1000.0 %",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedMediaItem.soundLevel = "1000.0 %"
                                        self.refreshData()
        }))
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.audioButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func refreshData() {
        self.tableView.reloadData()
    }
    
}

// MARK: Extension for IBActions
extension ConverterViewController {
    
    @IBAction func didTapDone(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapConvert(sender: UIButton) {
        //SVProgressHUD.show()
        self.loadingView.isHidden = false
        self.batchConvertItemsInArray()
    }
    
    @IBAction func didTapCancel(sender: UIButton) {
        self.loadingView.isHidden = true
        MobileFFmpeg.cancel()
        self.ffmpegCommandsArray.removeAllObjects()
        self.destinationFilePathsArray.removeAllObjects()
    }
    
}

// MARK: Extension for Table View
extension ConverterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mediaItemsArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        let model = self.mediaItemsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model, searchTerm: "")
        
        cell.audioStackView.isHidden = false
        cell.formatStackView.isHidden = false
        cell.moreImageView.isHidden = true
        
        cell.formatButton.isHidden = false
        cell.audioButton.isHidden = false
        cell.formatButton.tag = indexPath.row
        cell.audioButton.tag = indexPath.row
        cell.formatButton.addTarget(self, action:#selector(formatButtonPressed(sender:)), for: .touchUpInside)
        cell.audioButton.addTarget(self, action:#selector(audioButtonPressed(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    @objc func audioButtonPressed(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        convertButtonShouldBeEnabled = true
        self.selectedMediaItem = mediaItemsArray[sender.tag]
        
        self.openAudioSheet(mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        
    }
    
    @objc func formatButtonPressed(sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        convertButtonShouldBeEnabled = true
        self.selectedMediaItem = mediaItemsArray[sender.tag]
        
        self.openFormatSheet(mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        
    }
}

// MARK: Extension for Conversion
extension ConverterViewController {
    
    // Audio Methods Start
    func convertToAudioFormat(ffmpegCommand: String, destinationFilePath: String/*, downloadGroup: DispatchGroup*/, mediaItem: MediaItems) {
        
        MobileFFmpegConfig.setLogDelegate(self)
        
        print("ffmpegCommand: \(ffmpegCommand)")
        
        let result = MobileFFmpeg.execute(ffmpegCommand)
        if result != RETURN_CODE_SUCCESS {
                   DispatchQueue.main.async {
                       self.loadingView.isHidden = true

                       print("stabilisation analyze process failed with result: \(result)")
                       self.ffmpegCommandsArray.removeAllObjects()
                       self.destinationFilePathsArray.removeAllObjects()
                       if result != RETURN_CODE_CANCEL {
                           CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Conversion process failed!", owner: self)
                       }
                       return
                   }
               }
        print("result: \(result)")
    }
    
    func exportFinalAudioToAppGallery(destinationFilePath: String, mediaItem: MediaItems) {
        
        guard let destinationFilePathURL  = URL.init(string: destinationFilePath) else { return }
        
        var duration = 0.0
        var fileSize = 0.0
        
        if ExportUtility.sharedInstance.isAudioSupportedByFFMPEG(destinationFilePath: destinationFilePath) {
            duration = CommonClass.sharedInstance.getDurationOfFileInSecondsUsingFFMPEG(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        else {
            duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        
        // Get their filesize from a different method
        if((destinationFilePath.range(of: "opus")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        else if((destinationFilePath.range(of: "ogg")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        
        let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItem.id)
        let audioExportModel = AudioExportModel()
        audioExportModel.id = ExportUtility.sharedInstance.getIDForNewAudioExportModel()
        audioExportModel.size = fileSize
        audioExportModel.duration = duration
        audioExportModel.filePath = "\(destinationFilePathURL.lastPathComponent)"
        audioExportModel.fileType = mediaItem.fileTypeToConvert
        audioExportModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
        audioExportModel.soundLevel = mediaItem.soundLevel
        let fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"

        let modelName = "\(audioModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"
        audioExportModel.name = modelName
        if mediaItem.fullName.count > 0 {
            audioExportModel.fullName = fullModelName
        }

        ExportUtility.sharedInstance.addAudioExportModelToDatabase(audioExportModel: audioExportModel)
        
    }
    
    // Audio Methods End
}

// MARK: Extension for Conversion
extension ConverterViewController {
    
    // Video Methods Start
    func convertToVideoFormat(ffmpegCommand: String, destinationFilePath: String/*, downloadGroup: DispatchGroup*/, mediaItem: MediaItems) {
        
        MobileFFmpegConfig.setLogDelegate(self)
        
        print("ffmpegCommand: \(ffmpegCommand)")
        let result = MobileFFmpeg.execute(ffmpegCommand)
        
        if result != RETURN_CODE_SUCCESS {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true

                print("stabilisation analyze process failed with result: \(result)")
                self.ffmpegCommandsArray.removeAllObjects()
                self.destinationFilePathsArray.removeAllObjects()
                if result != RETURN_CODE_CANCEL {
                    CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Conversion process failed!", owner: self)
                }
                return
            }
        }
        print("result: \(result)")
        
        
        
        
        
    }
    
    func exportFinalVideoToAppGallery(destinationFilePath: String, mediaItem: MediaItems) {
        
        guard let destinationFilePathURL  = URL.init(string: destinationFilePath) else { return }
        
        var duration = 0.0
        var fileSize = 0.0
        
        if ExportUtility.sharedInstance.isAudioSupportedByFFMPEG(destinationFilePath: destinationFilePath) {
            duration = CommonClass.sharedInstance.getDurationOfFileInSecondsUsingFFMPEG(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        else {
            duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationFilePathURL)
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationFilePathURL)
        }
        
        // Get their filesize from a different method
        if((destinationFilePath.range(of: "ogg")) != nil) {
            fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMBForOggAndOpus(url: destinationFilePathURL as URL)
        }
        
        let videoModel = ImportUtility.sharedInstance.getVideoModelForID(id: mediaItem.id)
        let videoExportModel = VideoExportModel()
        videoExportModel.id = ExportUtility.sharedInstance.getIDForNewVideoExportModel()
        videoExportModel.size = fileSize
        videoExportModel.duration = duration
        videoExportModel.filePath = "\(destinationFilePathURL.lastPathComponent)"
        videoExportModel.fileType = mediaItem.fileTypeToConvert
        videoExportModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
        videoExportModel.soundLevel = mediaItem.soundLevel
        
        var modelName = "\(videoModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"
        var fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]"
        
        
        if stabilizeVideo == true {
            modelName = "\(videoModel.name) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]-stabilized"
            fullModelName = "\(mediaItem.fullName) \(CommonClass.sharedInstance.getCurrentDateInString()) [\(mediaItem.soundLevel)]-stabilized"

        }
        videoExportModel.name = modelName
        videoExportModel.fullName = fullModelName

        ExportUtility.sharedInstance.addVideoExportModelToDatabase(videoExportModel: videoExportModel)
        
    }
    
    // Video Methods End
}

// MARK: Extension for Batch Convert
extension ConverterViewController {
    func batchConvertItemsInArray() {
        for mediaItem in self.mediaItemsArray {
            var sourceFilePath = ""
            var fileName = NSString()
            
            if self.selectedMediaType == .Audio {
                let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItem.id)
                sourceFilePath = audioModel.filePath
                fileName = audioModel.filePath as NSString
            } else {
                let model = ImportUtility.sharedInstance.getVideoModelForID(id: mediaItem.id)
                sourceFilePath = model.filePath
                fileName = model.filePath as NSString
            }
            
            var sourceFilePathURL = NSURL(string: sourceFilePath) ?? NSURL()
            
            let fileNameWithExt = sourceFilePath
            var sourceUrl = URL.init(string: "")
            
            if self.selectedMediaType == .Audio {
                sourceUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
            } else {
                sourceUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
            }
            
            sourceFilePathURL = sourceUrl! as NSURL
            
            if self.selectedMediaType == .Audio {
                let destinationFilePathURL = ExportUtility.sharedInstance.getDestinationFilePath(sourceFileUrl: sourceFilePathURL as URL, fileName: fileName.deletingPathExtension, fileExtension: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel)
                
                // Get ffmpeg command according to format
                let ffmpegCommand = ExportUtility.sharedInstance.getAudioFormatConvertFFMPEGCommand(extention: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel, sourceFilePath: sourceFilePathURL.absoluteString ?? "", destinationFilePath: destinationFilePathURL.absoluteString)
                
                ffmpegCommandsArray.add(ffmpegCommand)
                destinationFilePathsArray.add(destinationFilePathURL)
            } else {
                //let destinationFilePathURL = ExportUtility.sharedInstance.getDestinationVideoFilePath(sourceFileUrl: sourceFilePathURL as URL, fileName: fileName.deletingPathExtension, fileExtension: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel)
                let destinationFilePathURL = ExportUtility.sharedInstance.getDestinationVideoFilePath(sourceFileUrl: sourceFilePathURL as URL, fileName: mediaItem.fullName, fileExtension: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel)

                // Get ffmpeg command according to format
                let ffmpegCommand = ExportUtility.sharedInstance.getVideoFormatConvertFFMPEGCommand(extention: mediaItem.fileTypeToConvert, volumeBoost: mediaItem.soundLevel, sourceFilePath: sourceFilePathURL.absoluteString ?? "", destinationFilePath: destinationFilePathURL.absoluteString, musicFilePath: self.musicURL?.absoluteString ?? "", subtitleFilePath: self.subtitleName)//self.subtitleURL?.absoluteString ?? "")
                
                ffmpegCommandsArray.add(ffmpegCommand)
                destinationFilePathsArray.add(destinationFilePathURL)
            }
        }
        self.performFFMPEGConversion()
    }
    
    func performFFMPEGConversion() {
        
        var index = 0
        DispatchQueue.global(qos: .userInitiated).async {
            
            for _ in self.ffmpegCommandsArray {
                
                if index < self.ffmpegCommandsArray.count && index < self.destinationFilePathsArray.count && index < self.mediaItemsArray.count {
                    let ffmpegCommand = self.ffmpegCommandsArray[index] as? String
                    let destinationFilePathURL = self.destinationFilePathsArray[index] as? URL
                    let mediaItem = self.mediaItemsArray[index]
                    
                    if self.selectedMediaType == .Audio {
                        self.convertToAudioFormat(ffmpegCommand: ffmpegCommand ?? "", destinationFilePath: destinationFilePathURL?.absoluteString ?? "", mediaItem: mediaItem)
                    }
                    else {
                        self.convertToVideoFormat(ffmpegCommand: ffmpegCommand ?? "", destinationFilePath: destinationFilePathURL?.absoluteString ?? "", mediaItem: mediaItem)
                    }
                }
                
                index = index+1
                
            }
            
            if self.selectedMediaType == .Audio {
                DispatchQueue.main.async {
                    print("all done")
                    if self.loadingView.isHidden == true {
                        return
                    }
                    for i in 0..<self.mediaItemsArray.count {
                        // Export to Realm DB
                        self.exportFinalAudioToAppGallery(destinationFilePath: (self.destinationFilePathsArray[i] as? URL)?.absoluteString ?? "", mediaItem: self.mediaItemsArray[i])
                        // Update Gallery
                        NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
                    }
                    self.loadingView.isHidden = true

                    //SVProgressHUD.dismiss()
                    
                    let alert = UIAlertController(title: "Successfully Exported", message: "You can see the exported file/s in Gallery", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                        self.dismiss(animated: true) {
                            self.delegate?.didConvertedAudio()
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                self.performVideoStabilization()
            }
        }
    }
    
}

// MARK: Extension for Log Delegate
extension ConverterViewController: LogDelegate {
    
    func logCallback(_ level: Int32, _ message: String!) {
        print("FFMPEG - message from FFMPEG: \(message ?? "")")
    }
    
}

// MARK: Video Work
extension ConverterViewController {
    
    @IBAction func stabilizeVideoSwitchPressed(sender: UISwitch) {
        stabilizeVideo = sender.isOn
        print("stabilizeVideo: \(stabilizeVideo)")
    }
    
    @IBAction func addMusicButtonPressed() {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
        }))
        sheet.addAction(UIAlertAction(title: "Device library",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.openMusicLibrary()
        }))
        
        sheet.addAction(UIAlertAction(title: "AV Enhance library",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.openAppAudioVC()
        }))
        
        //self.present(sheet, animated: true, completion: nil)
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: addMusicButton, owner: self, direction: .down)
        
    }
    
    
    func openAppAudioVC() {
        let vc = AppAudioGalleryViewController(nibName: "AppAudioGalleryViewController", bundle: nil)
        
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
    
    @IBAction func removeMusicButtonPressed() {
        self.musicNameLabel.text = "..."
        self.musicURL = nil
        self.removeMusicButton.isHidden = true
    }
    
    @IBAction func addSubtitleButtonPressed() {
        
        MobileFFmpeg.execute("-codecs")
        
        
        self.openFilesApp()
    }
    
    @IBAction func removeSubtitleButtonPressed() {
        self.subtitleLabel.text = "..."
        self.subtitleURL = nil
        self.subtitleName = ""
        self.removeSubtitleButton.isHidden = true
    }
    
    func openMusicLibrary() {
        let mediaPicker: MPMediaPickerController = MPMediaPickerController.self(mediaTypes: MPMediaType.music)
        mediaPicker.delegate = self
        mediaPicker.allowsPickingMultipleItems = false
        self.present(mediaPicker, animated: true, completion: nil)
    }
    
    func openFilesApp() {
        
        let controller = UIDocumentPickerViewController(
            documentTypes: ["public.item"], // choose your desired documents the user is allowed to select
            in: .import // choose your desired UIDocumentPickerMode
        )
        controller.delegate = self
        if #available(iOS 11.0, *) {
            controller.allowsMultipleSelection = false
        } else {
            // Fallback on earlier versions
        }
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
}

// MARK: Extension for Stabilisation
extension ConverterViewController {
    
    func performVideoStabilization() {
        
        if(self.stabilizeVideo) {
            self.buildTransformFile()
        }
        else {
            DispatchQueue.main.async {
                print("all done")
                if self.loadingView.isHidden == true {
                    return
                    
                }
                for i in 0..<self.mediaItemsArray.count {
                    // Export to Realm DB
                    self.exportFinalVideoToAppGallery(destinationFilePath: (self.destinationFilePathsArray[i] as? URL)?.absoluteString ?? "", mediaItem: self.mediaItemsArray[i])
                    // Update Gallery
                    NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
                }
                self.loadingView.isHidden = true
                //SVProgressHUD.dismiss()
                
                let alert = UIAlertController(title: "Successfully Exported", message: "You can see the exported file/s in Gallery", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                    self.dismiss(animated: true) {
                        self.delegate?.didConvertedAudio()
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    func buildTransformFile() {
        
        let videoFile = self.destinationFilePathsArray[0] as? URL
        let fileName = videoFile?.lastPathComponent.fileName()
        let pathExtension = videoFile?.pathExtension
        
        let shakeResultsFile = ExportUtility.sharedInstance.getTransformFilePath()
        let stabilizedVideoFile = ExportUtility.sharedInstance.getStabilizedVideoPath(fileName: fileName ?? "", pathExtension: pathExtension ?? "")
        
        DispatchQueue.main.async {
            //SVProgressHUD.dismiss()
            self.loadingView.isHidden = false

            print("done creating video, now moving to stabilising")
            
            let analyzeFFmpegCommand = "-hide_banner -y -i \(self.destinationFilePathsArray[0]) -vf vidstabdetect=stepsize=32:shakiness=10:accuracy=15:result=\(ExportUtility.sharedInstance.getTransformFilePath()) -f null -"
            
            //SVProgressHUD.show(withStatus: "Analyzing video for stabilisation")
            
            DispatchQueue.global(qos: .default).async {
                MobileFFmpegConfig.setLogDelegate(self)
                let result = MobileFFmpeg.execute(analyzeFFmpegCommand)
                
                print("stabilisation analyze process ended in result: \(result)")
                //SVProgressHUD.dismiss()

                DispatchQueue.main.async {
                    if result == RETURN_CODE_SUCCESS {
                        //SVProgressHUD.show(withStatus: "Stabilising video")
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = false
                        }
                        DispatchQueue.global(qos: .default).async {
                            let stabiliseFFmpegCommand = "-hide_banner -y -i \(self.destinationFilePathsArray[0]) -vf vidstabtransform=smoothing=30:input=\(shakeResultsFile) \(stabilizedVideoFile)"
                            print("stabiliseFFmpegCommand: \(stabiliseFFmpegCommand)")
                            
                            let result = MobileFFmpeg.execute(stabiliseFFmpegCommand)
                            print("stabilisation transform process ended in result: \(result)")
                            
                            DispatchQueue.main.async {
                                if result == RETURN_CODE_SUCCESS {
                                    print("stabilisation transform process completed successfully")
                                    if self.loadingView.isHidden == false {
                                        self.exportStabilisedVideoToAppGallery(stabilizedVideoFile: stabilizedVideoFile)
                                    }
                                }
                                else {
                                    //SVProgressHUD.dismiss()
                                    self.ffmpegCommandsArray.removeAllObjects()
                                    self.destinationFilePathsArray.removeAllObjects()
                                    self.loadingView.isHidden = true
                                    if result != RETURN_CODE_CANCEL {
                                        CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Stabilisation transform process failed!", owner: self)
                                    }
                                    print("stabilisation transform process failed with result: \(result)")
                                }
                            }
                        }
                    }
                    else {
                        //SVProgressHUD.dismiss()
                        self.ffmpegCommandsArray.removeAllObjects()
                        self.destinationFilePathsArray.removeAllObjects()
                        self.loadingView.isHidden = true
                        if result != RETURN_CODE_CANCEL {
                            CommonClass.sharedInstance.showAlertWithTitle(title: "Error", message: "Stabilisation analyze process failed!", owner: self)
                        }

                        print("stabilisation analyze process failed with result: \(result)")
                    
                    }
                }
            }
        }
        
    }
    
    func exportStabilisedVideoToAppGallery(stabilizedVideoFile: URL) {
        DispatchQueue.main.async {
            // Export to Realm DB
            self.exportFinalVideoToAppGallery(destinationFilePath: stabilizedVideoFile.absoluteString, mediaItem: self.mediaItemsArray[0])
            // Update Gallery
            NotificationCenter.default.post(name: Notification.Name("GalleryNeedsUpdate"), object: nil, userInfo: nil)
            //SVProgressHUD.dismiss()
            self.loadingView.isHidden = true
            
            let alert = UIAlertController(title: "Successfully Exported", message: "You can see the exported file/s in Gallery", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { (action) in
                self.dismiss(animated: true) {
                    self.delegate?.didConvertedAudio()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

// MARK: Extension for File Picker
extension ConverterViewController: MPMediaPickerControllerDelegate, ImportUtilityDelegate {
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        
        guard let mediaItem = mediaItemCollection.items.first else {
            NSLog("No item selected.")
            return
        }
        
        ImportUtility.sharedInstance.delegate = self
        
        // Get the song URL
        var songTitle = mediaItem.value(forProperty: MPMediaItemPropertyTitle) as? String ?? ""
        if let title = mediaItem.title {
            songTitle = title
            DispatchQueue.main.async {
                self.musicNameLabel.text = songTitle
            }
        }
        else {
            DispatchQueue.main.async {
                self.musicNameLabel.text = "Unknown Audio"
            }
        }
        
        self.dismiss(animated: true) {
            if let url = mediaItem.assetURL {
                DispatchQueue.main.async {
                    SVProgressHUD.show()
                }
                var originalFileName = ""
                
                // Song Title
                if(songTitle.count > 0) {
                    originalFileName = songTitle
                }
                else {
                    originalFileName = url.lastPathComponent.fileName()
                }
                
                let originalFileNameWithoutSpaces = originalFileName.replacingOccurrences(of: " ", with: "-")
                
                let name = "\(originalFileNameWithoutSpaces)-\(CommonClass.sharedInstance.getCurrentDateInString())"

                ImportUtility.sharedInstance.exportBackgroundAudioMedia(assetURL: url as URL, fileName: name, originalFileName: originalFileName)
            }
        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func exportFinished(destinationUrl: URL, originalFileName: String?) {
        
        self.musicURL = destinationUrl
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()

            self.removeMusicButton.isHidden = false
        }
        
        print("self.musicURL: \(self.musicURL)")
        
    }
    
}

// MARK: Extension for Document Picker
extension ConverterViewController: UIDocumentPickerDelegate {
    
    @available(iOS 11.0, *)
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let subtitleUrl = urls[0]
        let subtitleFileName = subtitleUrl.lastPathComponent
        
        self.subtitleName = "Subtitle/\(subtitleFileName)"
        
        if((subtitleFileName.range(of: "srt")) != nil) || ((subtitleFileName.range(of: "ass")) != nil) {
            let destinationUrl = ImportUtility.sharedInstance.copySubtitleFileToDocumentsDirectory(sourceFileUrl: subtitleUrl as URL, fileName: subtitleFileName)
            self.subtitleURL = destinationUrl
            
            DispatchQueue.main.async {
                self.subtitleLabel.text = subtitleFileName
                self.removeSubtitleButton.isHidden = false
            }
        }
        else {
            DispatchQueue.main.async {
                CommonClass.sharedInstance.showAlertWithTitle(title: "Alert", message: "Please select a valid subtitle file.", owner: self)
            }
        }
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let subtitleUrl = url
        let subtitleFileName = subtitleUrl.lastPathComponent
        
        self.subtitleName = "Subtitle/\(subtitleFileName)"
        
        if((subtitleFileName.range(of: "srt")) != nil) || ((subtitleFileName.range(of: "ass")) != nil) {
            let destinationUrl = ImportUtility.sharedInstance.copySubtitleFileToDocumentsDirectory(sourceFileUrl: subtitleUrl as URL, fileName: subtitleFileName)
            self.subtitleURL = destinationUrl
            
            DispatchQueue.main.async {
                self.subtitleLabel.text = subtitleFileName
                self.removeSubtitleButton.isHidden = false
            }
        }
        else {
            DispatchQueue.main.async {
                CommonClass.sharedInstance.showAlertWithTitle(title: "Alert", message: "Please select a valid subtitle file.", owner: self)
            }
        }
        
    }
    
}
extension ConverterViewController: AppAudioGalleryViewControllerDelegate {
    func didSelectAudio(withMediaItems mediaItems: MediaItems) {
       
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
        
       let audioModel = ExportUtility.sharedInstance.getAudioExportModelForID(id: mediaItems.id)
        var songTitle = audioModel.name
        var filePath = audioModel.filePath
        var url = ExportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(filePath)

        
        if audioModel.id == 0 {
            let audioModel = ImportUtility.sharedInstance.getAudioModelForID(id: mediaItems.id)
            songTitle = audioModel.name
            filePath = audioModel.filePath
            url = ImportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(filePath)
        }
        

        
        
        //ImportUtility.sharedInstance.delegate = self
        
        DispatchQueue.main.async {
            self.musicNameLabel.text = songTitle
        }
        
        self.dismiss(animated: true) {
            self.musicURL = url
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()

                self.removeMusicButton.isHidden = false
            }
        }
    }
}
extension Dispatch {

    static func background(_ task: @escaping () -> ()) {
        Dispatch.global(qos: .background).async {
            task()
        }
    }

    static func main(_ task: @escaping () -> ()) {
        Dispatch.main.async {
            task()
        }
    }
}
