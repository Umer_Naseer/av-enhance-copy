//
//  GalleryViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class GalleryViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByLabel: UILabel!
    
    // Class Variables
    var searchTerm = String()
    
    var selectedMediaItem = MediaItems()
    var mediaItemsArray = [MediaItems]()
    var selectedSortType = SortBy.Name
    var filteredDataSource: [MediaItems] {
        
        if(searchTerm == "") {
            //return mediaItemsArray
            return self.sortArrayAccordingToSortType(array: self.mediaItemsArray)
        } else {
            let array =  mediaItemsArray.filter({$0.videoName.lowercased().contains(self.searchTerm.lowercased())})
            return self.sortArrayAccordingToSortType(array: array)
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.mapDataSource()
        self.setupUIViews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        self.reloadDataSource()
    }
}

// Private Methods
extension GalleryViewController {
    
    func setupUIViews() {
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.tableView.estimatedRowHeight = 58
        
        self.setSortByButtonTitle()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.galleryNeedsUpdate(notification:)), name: Notification.Name("GalleryNeedsUpdate"), object: nil)
    }
    
    func mapDataSource() {
        
        self.mediaItemsArray.removeAll()
        let audioExportModels = ExportUtility.sharedInstance.getAllAudioExportModelsFromDatabase()
        let videoExportModels = ExportUtility.sharedInstance.getAllVideoExportModelsFromDatabase()
        
        for audioExportModel in audioExportModels {
            let media = MediaItems()
            media.id = audioExportModel.id
            media.videoName = audioExportModel.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: audioExportModel.duration)
            media.videoDesc = "\(audioExportModel.size) mb"
            media.isAudio = true
            
            let fileExtension = audioExportModel.fileType
            media.fileType = fileExtension
            media.date = audioExportModel.dateCreated
            media.fullName = audioExportModel.fullName

            self.mediaItemsArray.append(media)
        }
        for each in videoExportModels {
            let media = MediaItems()
            media.id = each.id
            media.videoName = each.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: each.duration)
            media.videoDesc = "\(each.size) mb"
            media.isAudio = false
            
            let fileExtension = each.fileType
            media.fileType = fileExtension
            media.date = each.dateCreated
            media.fullName = each.fullName

            self.mediaItemsArray.append(media)
        }
    }
    
    func setSortByButtonTitle() {
        
        switch self.selectedSortType {
        case .Name:
            self.sortByLabel.text = "Sort by Name"
        case .Date:
            self.sortByLabel.text = "Sort by Date"
        case .Duration:
            self.sortByLabel.text = "Sort by Duration"
        case .Size:
            self.sortByLabel.text = "Sort by Size"
        }
        
    }
    
    func openItemSheet(item: MediaItems, mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Preview",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.previewAudioPressed(model: self.selectedMediaItem)
        }))
        sheet.addAction(UIAlertAction(title: "Share",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        if self.selectedMediaItem.isAudio {
                                            self.shareAudioPressed(id: self.selectedMediaItem.id, mediaTableViewCell: mediaTableViewCell)
                                        } else {
                                            self.shareVideoPressed(id: self.selectedMediaItem.id, mediaTableViewCell: mediaTableViewCell)
                                        }
        }))
        sheet.addAction(UIAlertAction(title: "Rename",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        if self.selectedMediaItem.isAudio {
                                            self.renameAudioPressed(id: self.selectedMediaItem.id)
                                        }   else {
                                            self.renameVideoPressed(id: self.selectedMediaItem.id)
                                        }
        }))
        sheet.addAction(UIAlertAction(title: "Delete",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        if self.selectedMediaItem.isAudio {
                                            self.deleteAudioPressed(id: self.selectedMediaItem.id)
                                        } else {
                                            self.deleteVideoPressed(id: self.selectedMediaItem.id)
                                        }
        }))
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.audioButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func sortArrayAccordingToSortType(array : [MediaItems]) -> [MediaItems]
    {
        var ascendingOrder = true
        if self.selectedSortType == .Date {
            ascendingOrder = false
        }
        
        let sortedArray = self.sortArray(usingArray: array, isAscending: ascendingOrder, sortingType: self.selectedSortType)
        
        return sortedArray
    }
    
    func sortArray(usingArray arrayToSort:[MediaItems], isAscending : Bool, sortingType : SortBy) -> [MediaItems] {
        var array = arrayToSort
        
        switch sortingType {
        case .Name:
            array = array.sorted(by: { $0.videoName.compare($1.videoName, options: .caseInsensitive, range: nil, locale: nil) == .orderedAscending })//array.sorted(by: { $0.videoName < $1.videoName })
            
        case .Date:
            array = array.sorted(by: { $0.date < $1.date })
        
        case .Duration:
            //array = array.sorted(by: { $0.videoLength < $1.videoLength })
            array = array.sorted(by: { CommonClass.sharedInstance.getSecondsFromString(string: $0.videoLength) < CommonClass.sharedInstance.getSecondsFromString(string: $1.videoLength) })

            
        case .Size:
            array = array.sorted(by: { ByteCountFormatter().string(fromByteCount: Int64($0.videoSize.count)) < ByteCountFormatter().string(fromByteCount: Int64($1.videoSize.count)) })
        }
        if isAscending == true {
            return array
        }
        else{
            return array.reversed()
        }
    }
    
}

extension GalleryViewController {
    
    @IBAction func didTapAdd(sender: UIButton) {
        
    }
    
    @IBAction func didTapSortBy(sender: UIButton) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Date",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Date
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Name",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Name
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Duration",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Duration
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: sender, owner: self)
        
    }
    
}

extension GalleryViewController {
    
    @objc func galleryNeedsUpdate(notification: Notification) {
        self.reloadDataSource()
    }
    
    func reloadDataSource() {
        // Refresh data source
        self.mapDataSource()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func previewAudioPressed(model: MediaItems) {
        if model.isAudio {
        let audioModel = ExportUtility.sharedInstance.getAudioExportModelForID(id: model.id)
        let songTitle = audioModel.name
        let filePath = audioModel.filePath
        
        let destinationUrl = ExportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(filePath)
        
        CommonClass.sharedInstance.showDLGPlayerViewController(filePath: destinationUrl, owner: self, previewMode: true, songTitle: songTitle)
        } else {
            let videoModel = ExportUtility.sharedInstance.getVideoExportModelForID(id: model.id)//getVideoModelForID(id: model.id)
            let songTitle = videoModel.name
            let filePath = videoModel.filePath
            var songUrl = URL.init(string: "")
            
            if((filePath.range(of: "assets-library")) != nil) {
                // its coming from Music App
                songUrl = URL.init(fileURLWithPath: filePath)//NSURL(string: filePath) ?? NSURL()
            }
            else {
                // it's from Files App
                let fileNameWithExt = filePath
                let destinationUrl = ExportUtility.sharedInstance.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
                songUrl = destinationUrl
            }
            
            // Open Preview Controller
            if let url = songUrl {
                CommonClass.sharedInstance.showDLGPlayerViewController(filePath: url, owner: self, previewMode: true, songTitle: songTitle)
            }
        }
    }
    
    func shareAudioPressed(id: Int, mediaTableViewCell: MediaTableViewCell) {
        
        let audioModel = ExportUtility.sharedInstance.getAudioExportModelForID(id: id)
        let songTitle = audioModel.name
        let filePath = audioModel.filePath
        var songUrl = NSURL()
        
        if((filePath.range(of: "ipod-library")) != nil) {
            // its coming from Music App
            songUrl = NSURL(string: filePath) ?? NSURL()
        }
        else {
            // it's from Files App
            let fileNameWithExt = filePath
            let destinationUrl = ExportUtility.sharedInstance.getDocumentsDirectoryWithAudioFolder().appendingPathComponent(fileNameWithExt)
            songUrl = destinationUrl as NSURL
        }
        
        print("songUrl: \(songUrl)")
        print("songTitle: \(songTitle)")
        if let finalUrl = self.createLink(toFileAtURL: songUrl as URL, withName: "\(songTitle).\((songUrl.pathExtension)!)"){
            let activityItems: [Any] = [finalUrl, "\(songTitle)"]
            let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
            
            activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
            
            self.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, label: mediaTableViewCell.titleLabel, sheet: activityViewController)
        }
        
    }
    
    func shareVideoPressed(id: Int, mediaTableViewCell: MediaTableViewCell) {
        
        let audioModel = ExportUtility.sharedInstance.getVideoExportModelForID(id: id)
        let songTitle = audioModel.name
        let filePath = audioModel.filePath
        var songUrl = URL.init(string: "")
        
        if((filePath.range(of: "assets-library")) != nil) {
            // its coming from Music App
            songUrl = URL.init(fileURLWithPath: filePath)//NSURL(string: filePath) ?? NSURL()
        }
        else {
            // it's from Files App
            let fileNameWithExt = filePath
            let destinationUrl = ExportUtility.sharedInstance.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            songUrl = destinationUrl
        }
        
        
        if let url = songUrl {
            print("songUrl: \(url)")
            print("songTitle: \(songTitle)")
            if let finalUrl = self.createLink(toFileAtURL: url, withName: "\(songTitle).\(url.pathExtension)"){
                let activityItems: [Any] = [finalUrl, "\(songTitle)"]
                let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
                
                activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
                
                activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
                
                self.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, label: mediaTableViewCell.titleLabel, sheet: activityViewController)
            }
            
            
        }
    }
    
    func showPopoverFrom(mediaTableViewCell: MediaTableViewCell, label: UILabel, sheet: UIActivityViewController) {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            self.present(sheet, animated: true, completion: nil)
        case .pad:
            /* Get the souce rect frame */
            let buttonFrame = label.frame
            var showRect    = mediaTableViewCell.convert(buttonFrame, to: self.tableView)
            showRect        = self.tableView.convert(showRect, to: self.view)
            showRect.origin.y -= 10
            
            if let popoverPresentationController = sheet.popoverPresentationController {
                popoverPresentationController.permittedArrowDirections = .up
                popoverPresentationController.sourceView = self.view
                popoverPresentationController.sourceRect = showRect
                self.present(sheet, animated: true, completion: nil)
            }
        case .unspecified:
            self.present(sheet, animated: true, completion: nil)
        default:
            print("")
        }
        
    }
    
    func renameAudioPressed(id: Int) {
        
        let audioModel = ExportUtility.sharedInstance.getAudioExportModelForID(id: id)
        
        let alertController = UIAlertController(title: "Rename", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = audioModel.name
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            ExportUtility.sharedInstance.updateNameForAudioExportModelForID(id: id, name: firstTextField.text ?? "")
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func renameVideoPressed(id: Int) {
        
        let audioModel = ExportUtility.sharedInstance.getVideoExportModelForID(id: id)
        
        let alertController = UIAlertController(title: "Rename", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = audioModel.name
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            ExportUtility.sharedInstance.updateNameForVideoExportModelForID(id: id, name: firstTextField.text ?? "")
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func deleteAudioPressed(id: Int) {
        
        let alertController = UIAlertController(title: "Are you sure you want to delete this Audio?", message: "", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { alert -> Void in
            ExportUtility.sharedInstance.deleteAudioExportModelForID(id: id)
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func deleteVideoPressed(id: Int) {
        
        let alertController = UIAlertController(title: "Are you sure you want to delete this Video?", message: "", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { alert -> Void in
            ExportUtility.sharedInstance.deleteVideoExportModelForID(id: id)
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension GalleryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filteredDataSource.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > self.filteredDataSource.count - 1 {return}
        
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        
        let model = self.filteredDataSource[indexPath.row]
        self.selectedMediaItem = model
        
        self.openItemSheet(item: self.filteredDataSource[indexPath.row], mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        if indexPath.row > self.filteredDataSource.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
            return cell
        }
        
        let model = self.filteredDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model,searchTerm: self.searchTerm)
        
        return cell
        
    }
    
    func createLink(toFileAtURL fileUrl : URL, withName fileName : String) -> URL!{
        let fileManager = FileManager.default
        let tempDirectoryURL = fileManager.temporaryDirectory
        let linkURL = tempDirectoryURL.appendingPathComponent(fileName)
        if fileManager.fileExists(atPath: linkURL.path){
            do {
                try fileManager.removeItem(at: linkURL)
            } catch {
                print("i dunno")
            }
        }
        do {
            try fileManager.linkItem(at: fileUrl, to: linkURL)
            return linkURL
        } catch {
            print("i dunno")
        }
        return nil
    }
}



/*
- (NSURL *)createLinkToFileAtURL:(NSURL *)fileURL withName:(NSString *)fileName {
    NSFileManager *fileManager = [NSFileManager defaultManager];

    // create a path in the temp directory with the fileName
    NSURL *tempDirectoryURL = [[NSFileManager defaultManager] temporaryDirectory];
    NSURL *linkURL = [tempDirectoryURL URLByAppendingPathComponent:fileName];

    // if the link already exists, delete it
    if ([fileManager fileExistsAtPath:linkURL.path]) {
        NSError *error;
        [fileManager removeItemAtURL:linkURL error:&error];
        if (error) {
            // handle the error
        }
    }

    // create a link to the file
    NSError *error;
    BOOL flag = [fileManager linkItemAtURL:fileURL toURL:linkURL error:&error];
    if (!flag || error) {
        // handle the error
    }
    return linkURL;
}
*/
