//
//  VideoViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import RealmSwift
import SVProgressHUD
import MobileCoreServices
import Photos

class VideoViewController: UIViewController {
    
    // IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    
    // Class Variables
    var searchTerm = String()
    var selectionMode = false
    
    var selectedMediaItem = MediaItems()
    var selectedMediaItemsArray: [MediaItems] = []
    
    var mediaItemsArray = [MediaItems]()
    var selectedSortType = SortBy.Name
    var filteredDataSource: [MediaItems] {
        
        if(searchTerm == "") {
            //return mediaItemsArray
            return self.sortArrayAccordingToSortType(array: self.mediaItemsArray)
        } else {
            let array =  mediaItemsArray.filter({$0.videoName.lowercased().contains(self.searchTerm.lowercased())})
            return self.sortArrayAccordingToSortType(array: array)
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.mapDataSource()
        self.setupUIViews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let tabbar = self.tabBarController {
            CommonClass.sharedInstance.setTabBarTitles(tabbarVC: tabbar)
        }
        
        self.selectedMediaItemsArray.removeAll()
        self.tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.importVideosFromItunesLibrary()
        
    }
    
    lazy var refreshControl: UIRefreshControl = {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
        
    }()
    
}

// Private Methods
extension VideoViewController {
    
    func setupUIViews() {
        
        self.tableView.tableFooterView = UIView()
        let nibName = UINib(nibName: "MediaTableViewCell", bundle:nil)
        
        self.tableView.register(nibName, forCellReuseIdentifier: "mediaCell")
        self.tableView.estimatedRowHeight = 58
        
        self.tableView.addSubview(self.refreshControl)
        
        self.setSortByButtonTitle()
        
    }
    
    func mapDataSource() {
        
        self.mediaItemsArray.removeAll()
        let videoModels = ImportUtility.sharedInstance.getAllVideoModelsFromDatabase()
        
        for videoModel in videoModels {
            
            let media = MediaItems()
            media.id = videoModel.id
            media.videoName = videoModel.name
            media.videoLength = CommonClass.sharedInstance.getFormattedStringForSeconds(seconds: videoModel.duration)
            media.videoDesc = "\(videoModel.size) mb"
            media.isAudio = false
            
            let fileExtension = videoModel.fileType.fileExtension()
            media.fileType = fileExtension
            media.date = videoModel.dateCreated
            media.fullName = videoModel.fullName
            
            self.mediaItemsArray.append(media)
            
        }
        
        updateUI()
        
    }
    
    func setSortByButtonTitle() {
        
        switch self.selectedSortType {
        case .Name:
            self.sortByLabel.text = "Sort by Name"
        case .Date:
            self.sortByLabel.text = "Sort by Date"
        case .Duration:
            self.sortByLabel.text = "Sort by Duration"
        case .Size:
            self.sortByLabel.text = "Sort by Size"
        }
        
    }
    
    func updateUI() {
        
        if(self.mediaItemsArray.count == 0) {
            self.selectButton.isHidden = true
        }
        else {
            self.selectButton.isHidden = true
        }
        
    }
    
    @objc func handleRefresh() {
        
        self.importVideosFromItunesLibrary()
        
    }
    
    func openItemSheet(item: MediaItems, mediaTableViewCell: MediaTableViewCell) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Boost Volume",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.openConverterVC()
        }))
        sheet.addAction(UIAlertAction(title: "Preview",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.previewVideoPressed(id: self.selectedMediaItem.id)
        }))
        sheet.addAction(UIAlertAction(title: "Rename",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.renameVideoPressed(id: self.selectedMediaItem.id)
        }))
        sheet.addAction(UIAlertAction(title: "Delete",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        self.deleteVideoPressed(id: self.selectedMediaItem.id)
        }))
        
        if(self.selectedMediaItemsArray.count > 1) {
            sheet.actions[2].isEnabled = false
            sheet.actions[3].isEnabled = false
            sheet.actions[4].isEnabled = false
        }
        
        CommonClass.sharedInstance.showPopoverFrom(mediaTableViewCell: mediaTableViewCell, forButton: mediaTableViewCell.audioButton, sheet: sheet, owner: self, tableView: self.tableView)
        
    }
    
    func openConverterVC() {
        let vc = ConverterViewController(nibName: "ConverterViewController", bundle: nil)
        vc.selectedMediaType = MediaType.Video
        if(self.selectedMediaItemsArray.count > 0) {
            vc.mediaItemsArray = self.selectedMediaItemsArray
        } else {
            vc.mediaItemsArray = [self.selectedMediaItem]
        }
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func sortArrayAccordingToSortType(array : [MediaItems]) -> [MediaItems]
    {
        var ascendingOrder = true
        if self.selectedSortType == .Date {
            ascendingOrder = false
        }
        
        let sortedArray = self.sortArray(usingArray: array, isAscending: ascendingOrder, sortingType: self.selectedSortType)
        
        return sortedArray
    }
    
    func sortArray(usingArray arrayToSort:[MediaItems], isAscending : Bool, sortingType : SortBy) -> [MediaItems] {
        var array = arrayToSort
        
        switch sortingType {
        case .Name:
            array = array.sorted(by: { $0.videoName.compare($1.videoName, options: .caseInsensitive, range: nil, locale: nil) == .orderedAscending })//array.sorted(by: { $0.videoName < $1.videoName })
            
        case .Date:
            array = array.sorted(by: { $0.date < $1.date })
            
        case .Duration:
            
            array = array.sorted(by: { CommonClass.sharedInstance.getSecondsFromStringVedio(string: $0.videoLength) < CommonClass.sharedInstance.getSecondsFromStringVedio(string: $1.videoLength) })
            
        case .Size:
            array = array.sorted(by: { ByteCountFormatter().string(fromByteCount: Int64($0.videoSize.count)) < ByteCountFormatter().string(fromByteCount: Int64($1.videoSize.count)) })
        }
        
        if isAscending == true {
            return array
        }
        else{
            return array.reversed()
        }
    }
    
    func importVideosFromItunesLibrary() {
        
        if let urls = FileManager.default.urls(for: .documentDirectory) {
            print("urls")
            var urlsToDelete = [URL]()
            for each in urls {
                if each.absoluteString.contains(".mp4") || each.absoluteString.contains(".mov") || each.absoluteString.contains(".wmv") || each.absoluteString.contains(".mpeg") || each.absoluteString.contains(".avi") || each.absoluteString.contains(".flv") || each.absoluteString.contains(".mkv") || each.absoluteString.contains(".mpg") || each.absoluteString.contains(".ogg") || each.absoluteString.contains(".webm") || each.absoluteString.contains(".m2ts") || each.absoluteString.contains(".m4v") || each.absoluteString.contains(".asf") || each.absoluteString.contains(".f4v") || each.absoluteString.contains(".qt") || each.absoluteString.contains(".rm") || each.absoluteString.contains(".rmvb") || each.absoluteString.contains(".avchd") || each.absoluteString.contains(".mts") || each.absoluteString.contains(".ts") || each.absoluteString.contains(".3gp") || each.absoluteString.contains(".3g2"){
                    print(each as Any)
                    urlsToDelete.append(each)
                    let extensionStr = each.lastPathComponent.components(separatedBy: ".").last! as String
                    
                    let songTitleWithoutExtension = (each.lastPathComponent ).replacingOccurrences(of: extensionStr, with: "").dropLast()
                    
                    
                    self.importVideoPressed(songTitle: String(songTitleWithoutExtension), songUrl: each)
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.refreshControl.endRefreshing()
                for each in urlsToDelete {
                    ImportUtility.sharedInstance.deleteFileFromDocumentsDirectory(fileName: each.lastPathComponent)
                }
            })
        }
        
    }
    
}

extension VideoViewController {
    
    @IBAction func didTapAdd(sender: UIButton) {
        
        let sheet = UIAlertController.init(title: "Import Video", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Photo Library",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.openPhotoLibrary()
                                        
        }))
        sheet.addAction(UIAlertAction(title: "Files App (ICloud)",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        self.openFilesApp()
                                        
        }))
        sheet.addAction(UIAlertAction(title: "Itunes File Sharing",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        
                                        let vc = WebViewViewController(nibName: "WebViewViewController", bundle: nil)
                                        vc.modalPresentationStyle = .fullScreen
                                        vc.selectedScreenType = SettingsRowType.ItunesFileSharing
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        
        }))
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: sender, owner: self)
        
    }
    
    @IBAction func didTapSelect(sender: UIButton) {
        
        if(selectionMode) {
            selectionMode = false
            selectButton.setTitle("Select", for: .normal)
        }
        else {
            selectionMode = true
            selectButton.setTitle("Done", for: .normal)
        }
        self.tableView.reloadData()
        
    }
    
    @IBAction func didTapSortBy(sender: UIButton) {
        
        let sheet = UIAlertController.init(title: "AV Enhance", message: "Please select an option", preferredStyle: UIAlertController.Style.actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { _ in
            //Cancel Action
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Date",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Date
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Name",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Name
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        sheet.addAction(UIAlertAction(title: "Sort by Duration",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.selectedSortType = .Duration
                                        self.setSortByButtonTitle()
                                        self.tableView.reloadData()
        }))
        
        CommonClass.sharedInstance.showSheet(sheet: sheet, sender: sender, owner: self)
        
    }
    
    func openPhotoLibrary() {
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .authorized:
            DispatchQueue.main.async {
                let imagePickerController = UIImagePickerController()
                imagePickerController.sourceType = .savedPhotosAlbum
                imagePickerController.delegate = self
                imagePickerController.mediaTypes = ["public.movie"]
                self.present(imagePickerController, animated: true, completion: nil)
            }
            
        // show your media picker
        case .denied:
            let alertController = UIAlertController(title: "Allow photo album access?", message: "Need your permission to access photo albums", preferredStyle: .alert)
            let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    // Fallback on earlier versions
                }
            }
            alertController.addAction(dismissAction)
            alertController.addAction(settingsAction)
            
            self.present(alertController, animated: true, completion: nil)
        // probably alert the user that they need to grant photo access
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized {
                    DispatchQueue.main.async {
                        let imagePickerController = UIImagePickerController()
                        imagePickerController.sourceType = .savedPhotosAlbum
                        imagePickerController.delegate = self
                        imagePickerController.mediaTypes = ["public.movie"]
                        self.present(imagePickerController, animated: true, completion: nil)
                    }
                    
                }
            })
        default: break
        // case .restricted: break
        // probably alert the user that photo access is restricted
        }
        
        
        
        
        
        
        
        
        
    }
    
    func openFilesApp() {
//        "public.movie",[kUTTypeMovie as String, kUTTypeVideo as String,
        let controller = UIDocumentPickerViewController(
            documentTypes: [kUTTypeData as String], // choose your desired documents the user is allowed to select
            in: .import // choose your desired UIDocumentPickerMode
        )
        controller.delegate = self
        if #available(iOS 11.0, *) {
            controller.allowsMultipleSelection = false
        } else {
            // Fallback on earlier versions
        }
        
        self.present(controller, animated: true, completion: nil)
        
    }
    
}

// MARK: Extension for Search
extension VideoViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.searchTerm = ""
            self.tableView.reloadData()
        } else {
            self.searchTerm = searchText.lowercased()
            self.tableView.reloadData()
        }
    }
}

// MARK: Extension for Import
extension VideoViewController: ImportUtilityDelegate {
    
    func importVideoPressed(songTitle: String, songUrl: URL) {
        
        ImportUtility.sharedInstance.delegate = self
        SVProgressHUD.show()
        
        var originalFileName = ""
        
        // Song Title
        if(songTitle.count > 0) {
            originalFileName = songTitle
        }
        else {
            originalFileName = songUrl.lastPathComponent.fileName()
        }
        
        let originalFileNameWithoutSpaces = originalFileName.replacingOccurrences(of: " ", with: "")
        //let originalFileNameWithoutSpaces = originalFileName
        
        var name = ""
        if originalFileNameWithoutSpaces.contains(".") {
            
            let fileUrl = URL(string: originalFileNameWithoutSpaces)
            if let named = fileUrl?.deletingPathExtension().lastPathComponent {
                name = named
            }
            //name = originalFileNameWithoutSpaces
        } else {
            name = originalFileNameWithoutSpaces
        }
        let fullFileName = originalFileName
        let ext = songUrl.absoluteString.components(separatedBy: ".").last ?? "Mov"
        
        // Get Destination URL
        if((songUrl.absoluteString.range(of: "assets-library")) != nil) {
            
            ImportUtility.sharedInstance.exportVideoMedia(assetURL: songUrl as URL, fileName: name, originalFileName: originalFileName)
            
        }
        else {
            let destinationUrl = ImportUtility.sharedInstance.copyFileToDocumentsDirectoryForVideo(sourceFileUrl: songUrl as URL, fileName: name)
            
            // Get duration
            let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
            let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
            
            let videoModel = VideoModel()
            videoModel.id = ImportUtility.sharedInstance.getIDForNewVideoModel()
            //            videoModel.name = "\(originalFileName.capitalized)-\(videoModel.id)"
            videoModel.name = "\(name.capitalized).\(ext)"
            videoModel.size = fileSize
            videoModel.duration = duration
            videoModel.filePath = "\(destinationUrl.lastPathComponent)"
            
            videoModel.fileType = destinationUrl.lastPathComponent
            videoModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            videoModel.fullName = fullFileName
            
            ImportUtility.sharedInstance.addVideoModelToDatabase(videoModel: videoModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
    }
    
    func exportFinished(destinationUrl: URL, originalFileName: String?) {
        
        // Get duration
        let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
        let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
        
        DispatchQueue.main.async {
            
            let videoModel = VideoModel()
            videoModel.id = ImportUtility.sharedInstance.getIDForNewVideoModel()
            videoModel.name = originalFileName?.capitalized ?? ""
            videoModel.size = fileSize
            videoModel.duration = duration
            videoModel.filePath = "\(destinationUrl.lastPathComponent)"
            
            videoModel.fileType = destinationUrl.lastPathComponent
            videoModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            
            ImportUtility.sharedInstance.addVideoModelToDatabase(videoModel: videoModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
    }
}

// MARK: Extension for Audio Player Delegate
extension VideoViewController: DLGPlayerViewControllerDelegate {
    
    func importAudioPressed(_ songTitle: String!, songUrl: URL!) {
        ImportUtility.sharedInstance.delegate = self
        SVProgressHUD.show()
        
        var originalFileName = ""
        
        let name = "Video-\(CommonClass.sharedInstance.getCurrentDateInString())"
        
        // Song Title
        if(songTitle.count > 0) {
            originalFileName = songTitle
        }
        else {
            originalFileName = songUrl.lastPathComponent.fileName()
        }
        
        // Get Destination URL
        if((songUrl.absoluteString.range(of: "assets-library")) != nil) {
            
            ImportUtility.sharedInstance.exportVideoMedia(assetURL: songUrl as URL, fileName: name, originalFileName: originalFileName)
            
        }
        else {
            let destinationUrl = ImportUtility.sharedInstance.copyFileToDocumentsDirectoryForVideo(sourceFileUrl: songUrl as URL, fileName: name)
            
            // Get duration
            let duration = CommonClass.sharedInstance.getDurationOfFileInSeconds(url: destinationUrl)
            let fileSize = CommonClass.sharedInstance.getFileSizeFromURLInMB(url: destinationUrl)
            
            let videoModel = VideoModel()
            videoModel.id = ImportUtility.sharedInstance.getIDForNewAudioModel()
            videoModel.name = originalFileName.capitalized
            videoModel.size = fileSize
            videoModel.duration = duration
            videoModel.filePath = "\(destinationUrl.lastPathComponent)"
            
            videoModel.fileType = destinationUrl.lastPathComponent
            videoModel.dateCreated = CommonClass.sharedInstance.getCurrentDate()
            
            ImportUtility.sharedInstance.addVideoModelToDatabase(videoModel: videoModel)
            
            self.reloadDataSource()
            SVProgressHUD.dismiss()
        }
        
    }
    
}

// MARK: Extension for UI Actions
extension VideoViewController {
    
    func reloadDataSource() {
        
        // Refresh data source
        self.mapDataSource()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
    
    func deleteVideoPressed(id: Int) {
        
        let alertController = UIAlertController(title: "Are you sure you want to delete this Video?", message: "", preferredStyle: UIAlertController.Style.alert)
        let saveAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { alert -> Void in
            ImportUtility.sharedInstance.deleteVideoModelForID(id: id)
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func renameVideoPressed(id: Int) {
        
        let videoModel = ImportUtility.sharedInstance.getVideoModelForID(id: id)
        
        let alertController = UIAlertController(title: "Rename", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = videoModel.name
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            ImportUtility.sharedInstance.updateNameForVideoModelForID(id: id, name: firstTextField.text ?? "")
            self.reloadDataSource()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func previewVideoPressed(id: Int) {
        
        let videoModel = ImportUtility.sharedInstance.getVideoModelForID(id: id)
        let songTitle = videoModel.name
        let filePath = videoModel.filePath
        var songUrl = URL.init(string: "")
        
        if((filePath.range(of: "assets-library")) != nil) {
            // its coming from Music App
            songUrl = URL.init(fileURLWithPath: filePath)//NSURL(string: filePath) ?? NSURL()
        }
        else {
            // it's from Files App
            let fileNameWithExt = filePath
            let destinationUrl = ImportUtility.sharedInstance.getDocumentsDirectoryWithVideoFolder().appendingPathComponent(fileNameWithExt)
            songUrl = destinationUrl
        }
        
        //        print("songUrl: \(songUrl)")
        
        // Open Preview Controller
        if let url = songUrl {
            self.showVideoPlayerViewController(songTitle: songTitle, songUrl: url, previewMode: true)
        }
        
    }
    
}

extension VideoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filteredDataSource.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = setupCell(indexPath: indexPath , tableView : tableView)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row > self.filteredDataSource.count - 1 {return}
        
        let mediaTableViewCell = tableView.cellForRow(at: indexPath) as? MediaTableViewCell
        
        let model = self.filteredDataSource[indexPath.row]
        self.selectedMediaItem = model
        
        if selectionMode {
            if self.checkIfMediaItemIsAlreadySelected(mediaItems: model) {
                self.removeFromSelectedArray(mediaItems: model)
            }
            else {
                selectedMediaItemsArray.append(model)
            }
            tableView.reloadData()
        } else {
            self.openItemSheet(item: self.filteredDataSource[indexPath.row], mediaTableViewCell: mediaTableViewCell ?? MediaTableViewCell())
        }
    }
    
    @objc func setupCell(indexPath: IndexPath , tableView: UITableView) -> UITableViewCell {
        
        if indexPath.row > self.filteredDataSource.count - 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
            return cell
        }
        
        let model = self.filteredDataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "mediaCell", for: indexPath) as! MediaTableViewCell
        cell.bindData(model: model,searchTerm: self.searchTerm)
        
        // Selection
        if(checkIfMediaItemIsAlreadySelected(mediaItems: model)) {
            cell.selectedImageView?.isHidden = false
        }
        else {
            cell.selectedImageView?.isHidden = true
        }
        
        return cell
    }
    
}

class FileInfo {
    // Return Media name
    static func getMediaName(info:[UIImagePickerController.InfoKey : Any]) -> String {

        if #available(iOS 11.0, *) {
            if let asset = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                let assetResources = PHAssetResource.assetResources(for: asset)
                let firstObj = assetResources.first?.originalFilename as! NSString
                print(firstObj)
                return firstObj as String
            }
        } else {
            return ""
            // Fallback on earlier versions
        }
        return ""
    }
}

// MARK: Extension for Photo Library Picker
extension VideoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let fileNameOriginal = FileInfo.getMediaName(info: info)
        if let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            var orgFileName = ""
            if let imageURL = info[.referenceURL] as? URL {
                let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
                if let firstObject = result.firstObject {
                    let fileName = firstObject.value(forKey: "filename")
                    orgFileName = fileName as! String
                    let fileUrl = URL(string: fileName as! String)
                    if let name = fileUrl?.deletingPathExtension().lastPathComponent {
                        print(name)
                        orgFileName = name
                    }
                }
            }
            
            if orgFileName.count == 0 {
                orgFileName = url.absoluteString.components(separatedBy: "-").last ?? "video"
            }
            
            if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
                
                if(previewBeforeImporting == "true") {
                    // Open Preview Controller
                    self.showVideoPlayerViewController(songTitle: fileNameOriginal == "" ? orgFileName : fileNameOriginal, songUrl: url, previewMode: false)
                } else {
                    // Import directly
                    self.importVideoPressed(songTitle: fileNameOriginal == "" ? orgFileName : fileNameOriginal, songUrl: url)
                }
                
            } else {
                // Import directly
                self.importVideoPressed(songTitle: fileNameOriginal == "" ? orgFileName : fileNameOriginal, songUrl: url)
            }
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

// MARK: Extension for Document Picker
extension VideoViewController: UIDocumentPickerDelegate {
    
    @available(iOS 11.0, *)
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        let songUrl = urls[0]
        
        
        var orgFileName = "video"
        orgFileName = songUrl.deletingPathExtension().lastPathComponent
        
        if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
            
            if(previewBeforeImporting == "true") {
                // Open Preview Controller
                self.showVideoPlayerViewController(songTitle: orgFileName, songUrl: songUrl, previewMode: false)
            }
            else {
                // Import directly
                self.importVideoPressed(songTitle: orgFileName, songUrl: songUrl)
            }
            
        }
        else {
            // Import directly
            self.importVideoPressed(songTitle: orgFileName, songUrl: songUrl)
        }
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let songUrl = url
        
        if let previewBeforeImporting = UserDefaults.standard.value(forKey: Constants.UserDefaults.k_Preview_Before_Importing) as? String {
            
            if(previewBeforeImporting == "true") {
                // Open Preview Controller
                self.showVideoPlayerViewController(songTitle: "video", songUrl: songUrl, previewMode: false)
            }
            else {
                // Import directly
                self.importVideoPressed(songTitle: "video", songUrl: songUrl)
            }
            
        }
        else {
            // Import directly
            self.importVideoPressed(songTitle: "video", songUrl: songUrl)
        }
        
    }
    
}

// MARK: Extension for Video Player
extension VideoViewController {
    
    func showVideoPlayerViewController(songTitle: String, songUrl: URL, previewMode: Bool) {
        
        let dlgPlayerViewController = DLGPlayerViewController()
        dlgPlayerViewController.url = songUrl.absoluteString
        dlgPlayerViewController.songUrl = songUrl
        dlgPlayerViewController.songTitle = songTitle
        dlgPlayerViewController.autoplay = true
        dlgPlayerViewController.preventFromScreenLock = true
        dlgPlayerViewController.previewMode = previewMode
        dlgPlayerViewController.delegate = self
        
        dlgPlayerViewController.modalPresentationStyle = .fullScreen
        
        self.present(dlgPlayerViewController, animated: true) {
            dlgPlayerViewController.close()
            dlgPlayerViewController.open()
        }
        
    }
    
}

// MARK: Extension for Selection
extension VideoViewController {
    
    func checkIfMediaItemIsAlreadySelected(mediaItems: MediaItems) -> Bool {
        
        var alreadyPresent = false
        
        if self.selectedMediaItemsArray.contains( where: { $0.id == mediaItems.id } ) == true {
            alreadyPresent = true
        }
        
        return alreadyPresent
        
    }
    
    func removeFromSelectedArray(mediaItems: MediaItems) {
        
        if let index = selectedMediaItemsArray.firstIndex(where: {$0.id == mediaItems.id}) {
            selectedMediaItemsArray.remove(at: index)
        }
        
    }
    
}

extension VideoViewController: ConverterVCDelegate {
    func didConvertedAudio() {
        self.selectedMediaItemsArray = []
        self.tabBarController?.selectedIndex = 2
    }
}

