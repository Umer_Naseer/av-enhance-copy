//
//  WebViewViewController.swift
//  AV enhance
//
//  Created by umer on 8/4/19.
//  Copyright © 2019 umer. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    
//    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var webView: UIWebView!
    
    var selectedScreenType = SettingsRowType.FeedBack

    override func viewDidLoad() {
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupUIVIews()
        
    }
    
}

extension WebViewViewController {
    
    func setupUIVIews() {
        
        switch self.selectedScreenType {
        case .FeedBack:
            self.titleLabel.text = "Help And Feedback"
            self.load("https://www.apple.com/feedback/")
        case .TermNCondition:
            self.titleLabel.text = "Terms And Conditions"
            self.load("https://en.wikipedia.org/wiki/Terms_of_service")
        case .PrivacyPolicy:
            self.titleLabel.text = "Privacy Policy"
            self.load("https://en.wikipedia.org/wiki/Privacy_policy")
        case .ItunesFileSharing:
                self.titleLabel.text = "Itunes File Sharing"
                self.load("https://support.apple.com/en-us/HT201301")
        default:
            print("")
        }
        
    }
    
    func load(_ urlString: String) {
        
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)//.load(request, mimeType: <#String#>, textEncodingName: <#String#>)
        }
        
    }
    
}

extension WebViewViewController {
    
    @IBAction func didTapBack(sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
